import { useEffect } from 'react';
import type { ReactNode } from 'react';
import classNames from 'classnames';
import styles from './Modal.module.scss';

interface ModalProps {
    active: boolean;
    setActive: any;
    children?: ReactNode;
    closeIcon: boolean;
}

interface KeyboardEvent {
    keyCode: number;
}

function Modal({ active, setActive, children, closeIcon }: ModalProps) {
    useEffect(() => {
        const closeModalEsc = (e: KeyboardEvent) => {
            if (e.keyCode === 27) {
                closeModal();
            }
        };
        window.addEventListener('keydown', closeModalEsc);
        return () => window.removeEventListener('keydown', closeModalEsc);
    }, []);

    const closeModal = () => {
        setActive(false);
        document.body.classList.remove('lock');
    };

    active ? document.body.classList.add('lock') : '';

    return (
        <div
            className={
                active ? classNames(styles.root, styles.open) : styles.root
            }
            onClick={() => closeModal()}
        >
            <div className={styles.body}>
                <div
                    onClick={(e) => e.stopPropagation()}
                    className={
                        active
                            ? classNames(styles.content, styles.contentOpen)
                            : styles.content
                    }
                >
                    {closeIcon && (
                        <span
                            className={styles.close}
                            onClick={() => closeModal()}
                        ></span>
                    )}
                    {children}
                </div>
            </div>
        </div>
    );
}

export { Modal };
