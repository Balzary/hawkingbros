import { useEffect, useState } from 'react';
import Link from 'next/link';
import classNames from 'classnames';
import styles from './Footer.module.scss';
import { NavList } from './NavList';
import { getCities, declination } from '../../utils';

interface IItems {
    id: number;
    href: string;
    name: string;
    isTitle: boolean;
}

function Footer() {
    const [navItems, setNavItems] = useState<IItems[][]>();

    let items = [
        [
            {
                id: 1,
                href: '#',
                name: 'Коттеджи и усадьбы',
                isTitle: true,
            },
            {
                id: 2,
                href: '#',
                name: 'Бани и сауны',
                isTitle: true,
            },
            {
                id: 3,
                href: '#',
                name: 'Авто на прокат',
                isTitle: true,
            },
        ],
        [
            {
                id: 1,
                href: '#',
                name: 'Квартиры',
                isTitle: true,
            },
        ],
        [],
        [
            {
                id: 1,
                href: '/news',
                name: 'Новости',
                isTitle: false,
            },
            {
                id: 2,
                href: '#',
                name: 'Размещение и тарифы',
                isTitle: false,
            },
            {
                id: 3,
                href: '#',
                name: 'Объявления на карте',
                isTitle: false,
            },
            {
                id: 4,
                href: '/contacts',
                name: 'Контакты',
                isTitle: false,
            },
        ],
    ];

    const socialItems = [
        {
            id: 1,
            href: '#',
            icon: 'icon-4',
        },
        {
            id: 2,
            href: '#',
            icon: 'icon-5',
        },
        {
            id: 3,
            href: '#',
            icon: 'icon-6',
        },
    ];

    useEffect(() => {
        const func = (item: { label: string; value: string }) => {
            return {
                id: items[1].length + 1,
                href: `/${item.label}`,
                name: `Квартиры в ${declination(item.label, 'предложный')}`,
                isTitle: false,
            };
        };
        getCities().map((item, index) => {
            index % 2 === 0
                ? items[1].push(func(item))
                : items[2].push(func(item));
        });
        setNavItems(items);
    }, []);

    return (
        <footer className={styles.root}>
            <div className={classNames('container', styles.container)}>
                <div className={styles.copyright}>
                    <Link href="/">
                        <img src="/logo.png" />
                    </Link>
                    <span>сдаем бай</span>
                    <p>
                        ИП Шушкевич Андрей Викторович
                        <br />
                        УНП 192602485 Минским горисполкомом
                        <br />
                        10.02.2016
                        <br />
                        220068, РБ, г. Минск, ул. Осипенко, 21, кв.23
                        <br />
                        +375 29 621 48 33, sdaem@sdaem.by
                        <br />
                        Режим работы: 08:00-22:00
                    </p>
                </div>
                <div className={styles.block}>
                    <div className={styles.wrapper}>
                        {navItems &&
                            navItems.map((item, index) => (
                                <NavList key={index} navItems={item} />
                            ))}
                    </div>
                    <div className={styles.flex}>
                        <div className={styles.socialBlock}>
                            <span className={classNames('text', styles.text)}>
                                Мы в соцсетях
                            </span>
                            <ul className={styles.socialLinks}>
                                {socialItems.map((item) => (
                                    <li
                                        key={item.id}
                                        className={styles.socialItem}
                                    >
                                        <a
                                            href={item.href}
                                            className={classNames(
                                                styles.socialLink,
                                                item.icon
                                            )}
                                        ></a>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <div className={styles.images}>
                            <img src="/2.png" alt="" />
                            <img src="/3.png" alt="" />
                            <img src="/4.png" alt="" />
                            <img src="/5.png" alt="" />
                            <img src="/6.png" alt="" />
                            <img src="/7.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
}

export { Footer };
