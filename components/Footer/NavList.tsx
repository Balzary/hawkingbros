import Link from 'next/link';
import classNames from 'classnames';
import styles from './Footer.module.scss';

interface NavListProps {
    navItems: INavList[];
}

interface INavList {
    id: number;
    href: string;
    name: string;
    isTitle: boolean;
}

function NavList({ navItems }: NavListProps) {
    return (
        <ul className={styles.list}>
            {navItems.map((item) => (
                <li key={item.id} className={styles.listItem}>
                    <Link
                        href={item.href}
                        className={classNames(
                            styles.listLink,
                            item.isTitle && styles.title
                        )}
                    >
                        {item.name}
                    </Link>
                </li>
            ))}
        </ul>
    );
}

export { NavList };
