import type { ReactNode } from 'react';
import { Header } from './Header';
import { Footer } from './Footer';

interface LayoutProps {
    children?: ReactNode;
}

const Layout = ({ children }: LayoutProps) => (
    <>
        <Header />
        <main className="main">{children}</main>
        <Footer />
    </>
);

export default Layout;
