import classNames from 'classnames';
import styles from './SocialLinks.module.scss';

interface SocialLinksProps {
    bgColor: string;
}

function SocialLinks({ bgColor }: SocialLinksProps) {
    const socialItems = [
        {
            id: 1,
            href: '#',
            icon: 'icon-5',
        },
        {
            id: 2,
            href: '#',
            icon: 'icon-7',
        },
        {
            id: 3,
            href: '#',
            icon: 'icon-8',
        },
        {
            id: 4,
            href: '#',
            icon: 'icon-9',
        },
        {
            id: 5,
            href: '#',
            icon: 'icon-10',
        },
    ];
    return (
        <div className={styles.social}>
            <span className="text">Поделиться</span>
            <ul className={styles.socialLinks}>
                {socialItems.map((item) => (
                    <li key={item.id} className={styles.socialItem}>
                        <a
                            style={{ background: bgColor }}
                            href={item.href}
                            className={classNames(styles.socialLink, item.icon)}
                        ></a>
                    </li>
                ))}
            </ul>
        </div>
    );
}

export { SocialLinks };
