import PropTypes from 'prop-types';
import Link from 'next/link';
import styles from './Card.module.scss';
import { ICard } from '../../../types/types';

interface CardProps {
    card: ICard;
}

function Card({ card }: CardProps) {
    return (
        <div className={styles.card}>
            <img src={card.img} />
            <div className={styles.cardContent}>
                <span className={styles.cardTitle}>{card.title}</span>
                <p className={styles.cardText}>{card.description}</p>
                <div className={styles.cardFlex}>
                    <span className={styles.cardDate}>{card.date}</span>
                    <Link href={`/news/${card.id}`}>
                        <button className={styles.cardBtn}>Читать</button>
                    </Link>
                </div>
            </div>
        </div>
    );
}

/*для 8-го ДЗ типизация на PropTypes
Card.PropTypes = {
    card: PropTypes.shape({
        img: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
        id: PropTypes.number.isRequired,
    }),
};*/

export { Card };
