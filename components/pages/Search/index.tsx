import { useEffect, useState } from 'react';
import classNames from 'classnames';
import styles from './Search.module.scss';

interface IOptions {
    filter: {
        value: string;
    };
}

interface SearchProps {
    options: IOptions;
    setOptions: any;
}

interface KeyboardEvent {
    key: string;
}

function Search({ options, setOptions }: SearchProps) {
    const [value, setValue] = useState('');

    const handleKey = (e: KeyboardEvent) => {
        if (e.key === 'Enter') {
            handleSubmit();
        }
    };

    const handleSubmit = () => {
        setOptions({
            filter: {
                ...options.filter,
                value: value,
            },
        });
    };

    useEffect(() => {
        value === '' && handleSubmit();
    }, [value]);

    return (
        <div className={styles.search}>
            <input
                value={value}
                onChange={(e) => setValue(e.target.value)}
                onKeyDown={handleKey}
                className={styles.searchInput}
                placeholder="Поиск по статьям"
            />
            <button
                onClick={handleSubmit}
                className={classNames(styles.searchBtn, 'icon-3')}
            ></button>
        </div>
    );
}

export { Search };
