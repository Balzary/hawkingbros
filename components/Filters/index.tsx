import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import classNames from 'classnames';
import { CustomSelect } from '../CustomSelect';
import { Checkbox } from '../Checkbox';
import { checkbox, slPlaces } from '../../data/settings';
import styles from '../../pages/Home.module.scss';
import { getAreas, getMetro } from '../../utils';
import { IOptions } from '../../types/types';

interface FiltersProps {
    citie: string | undefined;
    active: boolean;
    setOptions: any;
    options: IOptions;
    style?: string;
    query: any;
    flag: boolean;
    reset?: boolean;
    setReset?: any;
    reset2?: boolean;
    setReset2?: any;
}

interface ISelect {
    value: string;
    label: string;
}

function Filters({
    citie,
    active,
    setOptions,
    options,
    style,
    query,
    flag,
    reset,
    setReset,
    reset2,
    setReset2,
}: FiltersProps) {
    const [areas, setAreas] = useState<ISelect[]>([]);
    const [areaActive, setAreaActive] = useState(
        query.area && { value: query.area, label: query.area }
    );
    const [metro, setMetro] = useState<ISelect[]>([]);
    const [metroActive, setMetroActive] = useState(
        query.metro && { value: query.metro, label: query.metro }
    );
    const [slPl, setSlPl] = useState(
        query.slPlaces && { value: query.slPlaces, label: query.slPlaces }
    );

    const router = useRouter();

    useEffect(() => {
        setAreas(getAreas(citie));
    }, [citie]);

    useEffect(() => {
        if (reset2) {
            setMetroActive('');
            setAreaActive('');
            setReset2(false);
        }
    }, [reset2]);

    useEffect(() => {
        if (reset) {
            setAreaActive('');
            setMetroActive('');
            setSlPl('');
            setReset(false);
            setMetro([]);
        }
    }, [reset]);

    useEffect(() => {
        //@ts-ignore
        areaActive && setMetro(getMetro(citie, areaActive.value));
    }, [areaActive]);

    useEffect(() => {
        if (metroActive || areaActive) {
            setOptions({
                ...options,
                filter: {
                    ...options.filter,
                    metro: metroActive ? metroActive.value : '',
                    area: areaActive ? areaActive.value : '',
                },
            });
        }
    }, [metroActive, areaActive]);

    return (
        <div
            style={active ? { display: 'block' } : { display: 'none' }}
            className={classNames(styles.filtersMore, style)}
        >
            <div className="container">
                <div className={styles.filtersContainer}>
                    <div>
                        <div
                            className={classNames('text', styles.filtersText)}
                            style={{ marginBottom: '10px' }}
                        >
                            Спальные места
                        </div>
                        <CustomSelect
                            placeholder="Выберите"
                            width="200px"
                            height="37px"
                            options={slPlaces}
                            portalTarget={true}
                            value={slPl}
                            setValue={(e: any) => {
                                flag &&
                                    router.push(
                                        {
                                            pathname: `/${query.id}`,
                                            query: {
                                                ...router.query,
                                                slPlaces: e.value,
                                            },
                                        },
                                        undefined,
                                        { scroll: false, shallow: true }
                                    );
                                setSlPl(e);
                                setOptions({
                                    ...options,
                                    filter: {
                                        ...options.filter,
                                        slPlaces: e.value,
                                    },
                                });
                            }}
                        />
                    </div>
                    <div>
                        <div
                            className={classNames('text', styles.filtersText)}
                            style={{ marginBottom: '10px' }}
                        >
                            Район
                        </div>
                        <CustomSelect
                            placeholder="Выберите"
                            width="200px"
                            height="37px"
                            options={areas}
                            value={areaActive}
                            portalTarget={true}
                            disabled={areas.length === 0}
                            setValue={(e: any) => {
                                flag &&
                                    router.push(
                                        {
                                            pathname: `/${query.id}`,
                                            query: {
                                                ...router.query,
                                                area: e.value,
                                            },
                                        },
                                        undefined,
                                        { scroll: false, shallow: true }
                                    );
                                setMetroActive('');
                                setAreaActive(e);
                            }}
                        />
                    </div>
                    <div>
                        <div
                            className={classNames('text', styles.filtersText)}
                            style={{ marginBottom: '10px' }}
                        >
                            Метро
                        </div>
                        <CustomSelect
                            placeholder="Выберите"
                            width="200px"
                            height="37px"
                            disabled={metro.length === 0}
                            options={metro}
                            value={metroActive}
                            portalTarget={true}
                            setValue={(e: any) => {
                                flag &&
                                    router.push(
                                        {
                                            pathname: `/${query.id}`,
                                            query: {
                                                ...router.query,
                                                metro: e.value,
                                            },
                                        },
                                        undefined,
                                        { scroll: false, shallow: true }
                                    );
                                setMetroActive(e);
                            }}
                        />
                    </div>
                </div>
                <div className={styles.filtersCheckbox}>
                    {checkbox.map((item) => (
                        <Checkbox
                            key={item}
                            text={item}
                            setOptions={setOptions}
                            options={options}
                            flag={flag}
                        />
                    ))}
                </div>
            </div>
        </div>
    );
}

export { Filters };
