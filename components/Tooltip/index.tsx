import classNames from 'classnames';
import type { ReactNode } from 'react';
import styles from './Tooltip.module.scss';

interface TooltipProps {
    children?: ReactNode;
    style?: string;
    tooltipActive?: boolean;
}

function Tooltip({ children, style, tooltipActive }: TooltipProps) {
    return (
        <div
            className={classNames(
                styles.tooltiptext,
                tooltipActive && styles.tooltipActive
            )}
            id={style}
        >
            {children}
        </div>
    );
}

export { Tooltip };
