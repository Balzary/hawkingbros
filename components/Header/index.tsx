import { useEffect, useRef, useState } from 'react';
import Reaptcha from 'reaptcha';
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { useSelector } from 'react-redux';
import { setCookie, destroyCookie } from 'nookies';
import classNames from 'classnames';
import { NavLink } from './NavLink';
import { Modal } from '../Modal';
import { Tooltip } from '../Tooltip';
import styles from './Header.module.scss';
import stylesInput from '../Input/Input.module.scss';
import stylesTooltip from '../Tooltip/Tooltip.module.scss';
import stylesContacts from '../../pages/contacts/Contacts.module.scss';
import $api from '../../http';
import { getCities } from '../../utils';

function Header() {
    const [navActive, setNavActive] = useState(false);
    const [loginActive, setLoginActive] = useState(false);
    const [regActive, setRegActive] = useState(false);
    const [success, setSuccess] = useState(false);
    const [verified, setVerified] = useState(false);

    //@ts-ignore
    const { dataUser } = useSelector((state) => state.sdaem);
    const [user, setUser] = useState(dataUser);

    const [cities, setCities] = useState<ISelect[] | ''>('');

    const navRef = useRef<HTMLDivElement | null>(null);

    const router = useRouter();

    interface ISelect {
        value: string;
        label: string;
    }

    const {
        register,
        handleSubmit,
        formState: { isValid, errors },
    } = useForm({
        defaultValues: {
            name: '',
            password: '',
            checkbox: false,
        },
        mode: 'onBlur',
    });

    const {
        register: register2,
        handleSubmit: handleSubmit2,
        formState: { isValid: isValid2, errors: errors2 },
    } = useForm({
        defaultValues: {
            name: '',
            email: '',
            password: '',
            repeatPassword: '',
        },
        mode: 'onBlur',
    });

    const onVerify = () => {
        setVerified(true);
    };

    const onSubmit = async (values: any) => {
        try {
            const res = await $api.post('/login', values);
            setUser(res.data);
            setLoginActive(false);
            setCookie(null, 'token', res.data.token, {
                maxAge: 30 * 24 * 60 * 60,
                path: '/',
            });
            document.body.classList.remove('lock');
            router.push(
                {
                    pathname: window.location.pathname,
                    query: {
                        ...router.query,
                    },
                },
                undefined,
                { scroll: false }
            );
        } catch (e: any) {
            console.log(e.response?.data?.message);
        }
    };

    const logout = async (e: Event) => {
        e.preventDefault();
        destroyCookie(null, 'token');
        setUser('');
        router.push(
            {
                pathname: window.location.pathname,
                query: {
                    ...router.query,
                },
            },
            undefined,
            { scroll: false }
        );
    };

    const onRegSubmit = async (values: any) => {
        try {
            if (verified) {
                setRegActive(false);
                setSuccess(true);
            }
        } catch (e: any) {
            console.log(e.response?.data?.message);
        }
    };

    const navItems = [
        [
            {
                href: '/',
                name: 'Главная',
                icon: '',
            },
            {
                href: '/news',
                name: 'Новости',
                icon: '',
            },
            {
                href: '#',
                name: 'Размещение и тарифы',
                icon: '',
            },
            {
                href: '#',
                name: 'Объявления на карте',
                icon: 'icon-1',
                styleIcon: styles.icon2,
            },
            {
                href: '/contacts',
                name: 'Контакты',
                icon: '',
            },
        ],
        [
            {
                href: '#',
                name: 'Квартиры на сутки',
                icon: 'icon-1',
                styleIcon: styles.icon1,
            },
            {
                href: '#',
                name: 'Коттеджи и усадьбы',
                icon: '',
            },
            {
                href: '#',
                name: 'Бани и Сауны',
                icon: '',
            },
            {
                href: '#',
                name: 'Авто напрокат',
                icon: '',
            },
        ],
    ];

    const clickHandler = () => {
        setNavActive(!navActive);
        navActive && document.body.classList.remove('lock');
    };

    useEffect(() => {
        setCities(getCities());
        const a = () => {
            if (window.innerWidth <= 768 && navRef.current) {
                //если окно просмотра меньше 768px то переносим блок с дополнительным меню в мобильное меню
                navRef.current.remove();
                const ab = document.querySelector(
                    `.${styles.navWrapper}`
                ) as HTMLDivElement;
                ab.append(navRef.current);
            }
            if (window.innerWidth > 768 && navRef.current) {
                //если окно просмотра больше 768px то переносим блок с дополнительным меню на свое место
                const ab = document.querySelector(
                    `.${styles.block}`
                ) as HTMLDivElement;
                ab.append(navRef.current);
            }
        };
        window.addEventListener('resize', a);
        a();
        return () => window.addEventListener('resize', a);
    }, []);

    return (
        <header>
            <div className={styles.root}>
                <div className={classNames(styles.nav, 'container')}>
                    <div
                        className={
                            navActive
                                ? classNames(
                                      styles.navIconActive,
                                      styles.navIcon
                                  )
                                : styles.navIcon
                        }
                        onClick={() => clickHandler()}
                    >
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div
                        className={
                            navActive
                                ? classNames(
                                      styles.navListActive,
                                      styles.navWrapper
                                  )
                                : styles.navWrapper
                        }
                    >
                        <ul className={classNames(styles.navList)}>
                            {navItems[0].map((item) => (
                                <NavLink
                                    key={item.name}
                                    item={item}
                                    styleItem={styles.navItem}
                                    styleLink={styles.navLink}
                                    styleIcon={item.styleIcon}
                                    setActive={setNavActive}
                                />
                            ))}
                        </ul>
                    </div>
                    <div className={styles.navBlock}>
                        <Link
                            href="/favorites"
                            className={classNames('text', styles.icon26)}
                        >
                            <svg
                                width="40"
                                height="37"
                                viewBox="0 0 40 37"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M29 0C25.52 0 22.18 1.62 20 4.18C17.82 1.62 14.48 0 11 0C4.84 0 0 4.84 0 11C0 18.56 6.8 24.72 17.1 34.08L20 36.7L22.9 34.06C33.2 24.72 40 18.56 40 11C40 4.84 35.16 0 29 0ZM20.2 31.1L20 31.3L19.8 31.1C10.28 22.48 4 16.78 4 11C4 7 7 4 11 4C14.08 4 17.08 5.98 18.14 8.72H21.88C22.92 5.98 25.92 4 29 4C33 4 36 7 36 11C36 16.78 29.72 22.48 20.2 31.1Z"
                                    fill="#F24E1E"
                                />
                            </svg>
                            Закладки
                        </Link>
                        <div className={styles.navLogin}>
                            {user ? (
                                <div
                                    className={stylesTooltip.tooltip}
                                    style={{ display: 'inline-flex' }}
                                >
                                    <div className={styles.avatar}>
                                        <img src={user.avatar} alt="" />
                                    </div>
                                    <b
                                        className={classNames(
                                            'text',
                                            styles.name,
                                            'icon-11'
                                        )}
                                    >
                                        {user.fullName}
                                    </b>
                                    <Tooltip style={styles.tooltip}>
                                        <a
                                            className={classNames('text')}
                                            id={styles.tooltipLogout}
                                            href=""
                                            onClick={(e: any) => {
                                                logout(e);
                                            }}
                                        >
                                            Выход
                                        </a>
                                    </Tooltip>
                                </div>
                            ) : (
                                <>
                                    <a
                                        href="#"
                                        className="text"
                                        onClick={() => setLoginActive(true)}
                                    >
                                        Вход
                                    </a>
                                    <span>|</span>
                                    <a
                                        href="#"
                                        className="text"
                                        onClick={() => setRegActive(true)}
                                    >
                                        Регистрация
                                    </a>
                                </>
                            )}
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.block}>
                <div
                    ref={navRef}
                    className={classNames('container', styles.blockNav)}
                >
                    <div className={styles.logo}>
                        <Link href="/">
                            <img src="/logo.png" alt="" />
                        </Link>
                    </div>
                    <ul className={styles.blockNavList}>
                        {navItems[1].map((item) => (
                            <NavLink
                                key={item.name}
                                item={item}
                                styleItem={styles.blockNavItem}
                                styleLink={styles.blockNavLink}
                                styleIcon={item.styleIcon}
                                setActive={setNavActive}
                                cities={cities}
                            />
                        ))}
                    </ul>
                    <button className={classNames(styles.btn, 'btn')}>
                        + Разместить объявление
                    </button>
                </div>
            </div>
            <Modal
                active={navActive}
                setActive={setNavActive}
                closeIcon={false}
            ></Modal>
            <Modal
                active={loginActive}
                setActive={setLoginActive}
                closeIcon={true}
            >
                <div className={styles.loginModal}>
                    <h2 className={classNames('subTitle', styles.subTitle)}>
                        Авторизация
                    </h2>
                    <div className={styles.loginBlock}>
                        <p className={classNames(styles.loginText, 'text')}>
                            Авторизируйтесь, чтобы начать публиковать свои
                            объявления
                        </p>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div
                                className={classNames(
                                    'icon-13',
                                    stylesInput.inputIcon
                                )}
                            >
                                <input
                                    className={stylesInput.input}
                                    placeholder="Логин"
                                    {...register('name', {
                                        required: true,
                                    })}
                                />
                            </div>
                            <div
                                className={classNames(
                                    'icon-16',
                                    stylesInput.inputIcon
                                )}
                            >
                                <input
                                    className={stylesInput.input}
                                    placeholder="Пароль"
                                    type="password"
                                    {...register('password', {
                                        required: true,
                                    })}
                                />
                            </div>
                            <div className={styles.loginFlex}>
                                <div className={styles.switchBlock}>
                                    <label className={styles.switch}>
                                        <input
                                            type="checkbox"
                                            {...register('checkbox')}
                                        />
                                        <span
                                            className={classNames(
                                                styles.slider,
                                                styles.round
                                            )}
                                        ></span>
                                    </label>
                                    <span className="text">Запомнить меня</span>
                                </div>
                                <a
                                    href="#"
                                    className={classNames(
                                        'text',
                                        styles.loginLink
                                    )}
                                >
                                    Забыли пароль?
                                </a>
                            </div>
                            <button
                                className={classNames(styles.loginBtn)}
                                type="submit"
                                disabled={!isValid}
                            >
                                Войти
                            </button>
                            <div className="text">
                                Еще нет аккаунта?{' '}
                                <a
                                    href="#"
                                    className={styles.loginLink}
                                    onClick={() => {
                                        setLoginActive(false);
                                        setRegActive(true);
                                    }}
                                >
                                    Создайте акканут
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </Modal>
            <Modal active={regActive} setActive={setRegActive} closeIcon={true}>
                <div className={classNames(styles.loginModal, styles.regModal)}>
                    <form
                        onSubmit={handleSubmit2(onRegSubmit)}
                        className={styles.regForm}
                    >
                        <h2 className={classNames('subTitle', styles.regTitle)}>
                            Регистрация
                        </h2>
                        <div
                            className={classNames(
                                'icon-13',
                                stylesInput.inputIcon
                            )}
                            style={
                                errors2?.name && {
                                    border: '2px solid #EB5757',
                                }
                            }
                        >
                            <input
                                className={stylesInput.input}
                                placeholder="Логин"
                                {...register2('name', {
                                    required: true,
                                })}
                            />
                        </div>
                        <div
                            className={classNames(
                                'icon-12',
                                stylesInput.inputIcon
                            )}
                            style={
                                errors2?.email && {
                                    border: '2px solid #EB5757',
                                }
                            }
                        >
                            <input
                                className={stylesInput.input}
                                placeholder="Электронная почта"
                                type="email"
                                {...register2('email', {
                                    required: true,
                                    pattern: {
                                        value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                        message: 'Введите корректный email',
                                    },
                                })}
                            />
                        </div>
                        <div
                            className={classNames(
                                'icon-16',
                                stylesInput.inputIcon
                            )}
                            style={
                                errors2?.password && {
                                    border: '2px solid #EB5757',
                                }
                            }
                        >
                            <input
                                className={stylesInput.input}
                                placeholder="Пароль"
                                type="password"
                                {...register2('password', {
                                    required: true,
                                })}
                            />
                        </div>
                        <div
                            className={classNames(
                                'icon-16',
                                stylesInput.inputIcon
                            )}
                            style={
                                errors2?.repeatPassword && {
                                    border: '2px solid #EB5757',
                                }
                            }
                        >
                            <input
                                className={stylesInput.input}
                                placeholder="Повторите пароль"
                                type="password"
                                {...register2('repeatPassword', {
                                    required: true,
                                })}
                            />
                        </div>
                        {Object.keys(errors2).length !== 0 && (
                            <div className={classNames(styles.error)}>
                                <span className="icon-17">Ошибка ввода</span>
                            </div>
                        )}
                        <Reaptcha
                            sitekey="6LdbXDohAAAAAOSRPg7cLWorWEB_GXXS9isiZ-eB"
                            onVerify={onVerify}
                        />
                        <button
                            className={classNames(
                                styles.loginBtn,
                                styles.regBtn
                            )}
                            type="submit"
                            disabled={!isValid2 || !verified}
                        >
                            Зарегистрироваться
                        </button>
                    </form>
                    <div className={styles.regBlock}>
                        <p className={styles.regText}>
                            Пользователь обязуется:
                        </p>
                        <ul className={styles.regText}>
                            <li className={styles.regListItm}>
                                предоставлять достоверную и актуальную
                                информацию при регистрации и добавлении объекта;
                            </li>
                            <li className={styles.regListItm}>
                                добавлять фотографии объектов соответствующие
                                действительности. Администрация сайта sdaem.by
                                оставляет за собой право удалять любую
                                информацию, размещенную пользователем, если
                                сочтет, что информация не соответствует
                                действительности, носит оскорбительный характер,
                                нарушает права и законные интересы других
                                граждан либо действующее законодательство
                                Республики Беларусь.
                            </li>
                        </ul>
                        <div className="text" style={{ marginTop: '70px' }}>
                            Уже есть аккаунт?{' '}
                            <a
                                href="#"
                                className={styles.loginLink}
                                onClick={() => {
                                    setLoginActive(true);
                                    setRegActive(false);
                                }}
                            >
                                Войдите
                            </a>
                        </div>
                    </div>
                </div>
            </Modal>
            <Modal active={success} setActive={setSuccess} closeIcon={true}>
                <div className={stylesContacts.modal}>
                    <h2
                        className={classNames(
                            'subTitle',
                            stylesContacts.modalTitle
                        )}
                    >
                        Подтвердите регистрацию
                    </h2>
                    <p className={classNames('text', stylesContacts.modalText)}>
                        Письмо для подтверждения аккаунта отправлено почту.
                        Перейдите по ссылке, указанной в письме. Если письма
                        нет, то проверьте спам.
                    </p>
                    <button
                        className={classNames(
                            stylesContacts.btn,
                            stylesContacts.btnYellow
                        )}
                        onClick={() => {
                            setSuccess(false);
                            document.body.classList.remove('lock');
                        }}
                    >
                        Закрыть окно
                    </button>
                </div>
            </Modal>
        </header>
    );
}

export { Header };
