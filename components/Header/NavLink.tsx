import Link from 'next/link';
import classNames from 'classnames';
import styles from './Header.module.scss';
import stylesTooltip from '../Tooltip/Tooltip.module.scss';
import { Tooltip } from '../Tooltip';
import { declination } from '../../utils';
import { useRouter } from 'next/router';
import { useState } from 'react';

interface IItem {
    name: string;
    href: string;
    icon: string;
}

interface NavLinkProps {
    item: IItem;
    styleItem: string;
    setActive: any;
    styleLink: string;
    styleIcon?: string;
    cities?: { value: string; label: string }[] | '';
}

function NavLink({
    item,
    styleItem,
    styleLink,
    styleIcon,
    setActive,
    cities,
}: NavLinkProps) {
    const [name, setName] = useState(item.name);
    const clickHandler = (e: any) => {
        if (e.target.classList.contains('Header_icon1__g5cB5')) {
            e.preventDefault();
        } else {
            setActive(false);
            document.body.classList.remove('lock');
        }
    };

    const router = useRouter();

    return (
        <li
            key={item.name}
            className={classNames(styleItem, stylesTooltip.tooltip)}
        >
            <Link
                className={classNames(
                    styleLink,
                    'text',
                    item.icon,
                    styleIcon,
                    decodeURI(router.asPath) === item.href ||
                        '/' + router.query.id === name
                        ? styles.linkActive
                        : ''
                )}
                href={item.href}
                onClick={(e) => clickHandler(e)}
            >
                {router.query.id ? name : item.name}
            </Link>
            {item.name === 'Квартиры на сутки' && (
                <Tooltip style={styles.tooltip2}>
                    <ul>
                        {cities &&
                            cities.map((item, index) => (
                                <li key={index} className="text">
                                    <Link
                                        href={`/${item.label}`}
                                        className={
                                            router.query.id === item.label
                                                ? styles.active
                                                : ''
                                        }
                                        onClick={(e) => {
                                            setName(
                                                `Квартиры в ${declination(
                                                    item.label,
                                                    'предложный'
                                                )}`
                                            );
                                            clickHandler(e);
                                        }}
                                    >
                                        Квартиры на сутки в{' '}
                                        {declination(item.label, 'предложный')}
                                    </Link>
                                </li>
                            ))}
                    </ul>
                </Tooltip>
            )}
        </li>
    );
}

export { NavLink };
