import classNames from 'classnames';
import styles from './Item.module.scss';
import { IApartments } from '../../types/types';

interface InfoProps {
    apartment: IApartments;
}

function Info({ apartment }: InfoProps) {
    return (
        <div className={styles.itemInfo}>
            <span className={classNames(styles.itemSpan, 'icon-13')}>
                {`${apartment.slPlaces} ${apartment.person}`}
            </span>
            <span className={styles.itemSpan}>{apartment.rooms} комн.</span>
            <span className={styles.itemSpan}>{apartment.square} м²</span>
        </div>
    );
}

export { Info };
