import { useEffect, useState } from 'react';
import classNames from 'classnames';
import { Tooltip } from '../Tooltip';
import Slider from 'react-slick';
import styles from './Item.module.scss';
import stylesTooltip from '../Tooltip/Tooltip.module.scss';
import { IApartments } from '../../types/types';
import { Info } from './info';
import { declination } from '../../utils';

interface ItemProps {
    isList?: boolean;
    isGold: boolean;
    style?: string;
    apartment: IApartments;
}

function Item({ apartment, style, isList, isGold }: ItemProps) {
    const [active, setActive] = useState(false);

    const settings = {
        dots: true,
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
    };

    const socialItems = [
        {
            id: 1,
            href: '#',
            icon: 'icon-8',
        },
        {
            id: 2,
            href: '#',
            icon: 'icon-10',
        },
        {
            id: 3,
            href: '#',
            icon: 'icon-12',
        },
    ];

    const clickHandler = () => {
        const storageItem = localStorage.getItem('apartment');
        if (storageItem) {
            const storage = JSON.parse(storageItem);
            const index = storage.findIndex(
                (item: IApartments) => item.id === apartment.id
            );
            if (index === -1) {
                localStorage.setItem(
                    'apartment',
                    JSON.stringify([...storage, apartment])
                );
                setActive(!active);
            } else {
                storage.splice(index, 1);
                localStorage.setItem('apartment', JSON.stringify([...storage]));
                setActive(!active);
            }
        } else localStorage.setItem('apartment', JSON.stringify([apartment]));
    };

    useEffect(() => {
        const storageItem = localStorage.getItem('apartment');
        if (storageItem) {
            const storage = JSON.parse(storageItem);
            if (
                storage.find(
                    (item: IApartments) => item.id === apartment.id
                ) !== undefined
            )
                setActive(true);
        }
    }, []);

    return (
        <div
            className={classNames(
                styles.item,
                style,
                isList && styles.item_list,
                isGold && styles.itemBlock
            )}
        >
            <Slider {...settings}>
                {apartment.image.map((item, index) => (
                    <div key={index} className={styles.itemImg}>
                        <img src={item} alt="" />
                    </div>
                ))}
            </Slider>
            <div className={styles.itemContent}>
                <div className={styles.itemFlex}>
                    {isList && (
                        <span className={styles.itemTitle}>
                            {apartment.rooms} комн. апартаменты{' '}
                            {apartment.metro
                                ? `на ${declination(
                                      apartment.metro,
                                      'местный'
                                  )}`
                                : ''}
                        </span>
                    )}
                    <div className={styles.itemPrice}>
                        {apartment.price} BYN
                        <div className="text">за сутки</div>
                    </div>
                    {!isList && <Info apartment={apartment} />}
                </div>
                <p className={classNames(styles.itemAddress, 'icon-1')}>
                    {apartment.address}
                </p>
                <div className={styles.itemWrapper}>
                    {isList && <Info apartment={apartment} />}
                    {apartment.metro && (
                        <span
                            className={classNames(
                                styles.itemAddress,
                                'icon-19'
                            )}
                            style={{ marginRight: '30px' }}
                        >
                            {apartment.metro}
                        </span>
                    )}
                    <span
                        className={classNames(
                            styles.itemAddress,
                            styles.itemArea
                        )}
                    >
                        {apartment.area}
                    </span>
                </div>
                <div className={styles.itemCheckbox}>
                    {apartment.checkbox.map((item, index) => (
                        <span key={index} className={styles.itemSpan}>
                            {item}
                        </span>
                    ))}
                </div>
                <p className={styles.itemText}>{apartment.description}</p>
                <div className={styles.itemButtons}>
                    {
                        <div className={styles.itemButtonsWrap}>
                            <span
                                onClick={() => {
                                    clickHandler();
                                }}
                            >
                                {active ? (
                                    <svg
                                        width="40"
                                        height="37"
                                        viewBox="0 0 40 37"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            d="M20 36.7L17.1 34.06C6.8 24.72 0 18.56 0 11C0 4.84 4.84 0 11 0C14.48 0 17.82 1.62 20 4.18C22.18 1.62 25.52 0 29 0C35.16 0 40 4.84 40 11C40 18.56 33.2 24.72 22.9 34.08L20 36.7Z"
                                            fill="rgba(235, 87, 87, 0.4)"
                                        />
                                    </svg>
                                ) : (
                                    <svg
                                        width="40"
                                        height="37"
                                        viewBox="0 0 40 37"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            d="M29 0C25.52 0 22.18 1.62 20 4.18C17.82 1.62 14.48 0 11 0C4.84 0 0 4.84 0 11C0 18.56 6.8 24.72 17.1 34.08L20 36.7L22.9 34.06C33.2 24.72 40 18.56 40 11C40 4.84 35.16 0 29 0ZM20.2 31.1L20 31.3L19.8 31.1C10.28 22.48 4 16.78 4 11C4 7 7 4 11 4C14.08 4 17.08 5.98 18.14 8.72H21.88C22.92 5.98 25.92 4 29 4C33 4 36 7 36 11C36 16.78 29.72 22.48 20.2 31.1Z"
                                            fill="#F24E1E"
                                        />
                                    </svg>
                                )}
                            </span>
                        </div>
                    }
                    <div
                        className={classNames(
                            stylesTooltip.tooltip,
                            styles.itemTooltip
                        )}
                    >
                        <button
                            className={classNames(
                                'btn',
                                styles.itemBtn,
                                'icon-14'
                            )}
                        >
                            Контакты
                        </button>
                        <Tooltip>
                            <div className={styles.tooltipImg}>
                                <img
                                    src="https://avatars.githubusercontent.com/u/45196989?v=4"
                                    alt=""
                                />
                            </div>
                            <div className={styles.tooltipBlock}>Владелец</div>
                            <p className={styles.tooltipName}>
                                {apartment.contacts.name}
                            </p>
                            <a
                                className={styles.tooltipName}
                                href="tel:+37529621-48-33"
                            >
                                {apartment.contacts.tel}
                            </a>
                            <a className={styles.tooltipEmail} href="mailto:">
                                {apartment.contacts.email}
                            </a>
                            <ul className={styles.socialLinks}>
                                {socialItems.map((item) => (
                                    <li
                                        key={item.id}
                                        className={styles.socialItem}
                                    >
                                        <a
                                            href={item.href}
                                            className={classNames(
                                                styles.socialLink,
                                                item.icon
                                            )}
                                        ></a>
                                    </li>
                                ))}
                            </ul>
                        </Tooltip>
                    </div>
                    <button className={classNames('btn', styles.itemBtn2)}>
                        Подробнее
                    </button>
                </div>
            </div>
        </div>
    );
}

export { Item };
