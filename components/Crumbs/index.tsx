import { useRouter } from 'next/router';
import Link from 'next/link';
import classNames from 'classnames';
import styles from './Crumbs.module.scss';

interface CrumbsProps {
    crumbsItems: ICrumbs[];
}

interface ICrumbs {
    id: number;
    href: string;
    name: string;
}

function Crumbs({ crumbsItems }: CrumbsProps) {
    const router = useRouter();

    return (
        <ul className={styles.crumbs}>
            <li className={styles.crumbsItem}>
                <Link
                    className={classNames(styles.crumbsLink, 'text', 'icon-2')}
                    href="/"
                ></Link>
            </li>
            {crumbsItems.map((item) => (
                <li
                    key={item.id}
                    className={classNames(styles.crumbsItem, 'text')}
                >
                    {decodeURI(router.asPath) === item.href ||
                    '/' + router.query.id === item.href ? (
                        item.name
                    ) : (
                        <Link className={styles.crumbsLink} href={item.href}>
                            {item.name}
                        </Link>
                    )}
                </li>
            ))}
        </ul>
    );
}

export { Crumbs };
