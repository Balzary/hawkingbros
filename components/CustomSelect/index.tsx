import Select from 'react-select';
import { useId } from 'react';

interface ISelect {
    value: string;
    label: string;
}

interface CustomSelectProps {
    placeholder: string;
    isSearchable?: boolean;
    options: ISelect[];
    setValue: any;
    disabled?: boolean;
    default?: ISelect;
    value?: ISelect | '';
    portalTarget?: any;
    width: string;
    height: string;
    bg?: string;
    shadow?: string;
}

function CustomSelect(props: CustomSelectProps) {
    const styles = {
        fontWeight: '500',
        fontSize: '14px',
        lineHeight: '17px',
        textAlign: 'left',
    };

    const customStyles = {
        placeholder: (provided: any) => ({
            ...provided,
            color: '#686868',
        }),
        control: (provided: any, state: any) => ({
            ...provided,
            ...styles,
            borderRadius: '20px',
            boxShadow: props.shadow ? props.shadow : 'none',
            border: '2px solid transparent',
            background: props.bg ? props.bg : '#F8F8F8',
            padding: '0 2px',
            transition: 'all 0.3s ease 0s',
            '&:hover': {
                //border: '2px solid rgba(102, 78, 249, 0.8)',
                cursor: 'pointer',
                background: 'rgba(102, 78, 249, 0.1)',
            },
        }),
        container: (provided: any) => ({
            ...provided,
            width: props.width,
        }),
        option: (provided: any, state: any) => ({
            ...provided,
            ...styles,
            cursor: 'pointer',
            padding: '10px 0 10px 15px',
            backgroundColor: state.isSelected && '#F8F8F8',
            color: state.isSelected && '#1E2123',
            '&:active': {
                backgroundColor: '#F8F8F8',
            },
            '&:hover': {
                backgroundColor: '#F8F8F8',
            },
        }),
        menu: (provided: any) => ({
            ...provided,
            borderRadius: '10px',
            boxShadow: '0px 0px 40px rgba(135, 124, 202, 0.3)',
            overflow: 'hidden',
            maxHeight: '185px',
        }),
        menuList: (provided: any) => ({
            ...provided,
            maxHeight: '185px',
        }),
        noOptionsMessage: (provided: any) => ({
            ...provided,
            textAlign: 'left',
            fontSize: '12px',
            color: '#cccccc',
        }),
    };

    return (
        <Select
            instanceId={useId()}
            styles={customStyles}
            options={props.options}
            placeholder={props.placeholder}
            isSearchable={props.isSearchable}
            value={props.value}
            defaultValue={props.default}
            onChange={props.setValue}
            isDisabled={props.disabled}
            noOptionsMessage={() => 'Ничего не найдено'}
            onMenuClose={() => {
                const tabs = document.querySelector('.tabs');
                tabs && tabs.classList.remove('lock');
            }}
            onMenuOpen={() => {
                const tabs = document.querySelector('.tabs');
                tabs && tabs.classList.add('lock');
            }}
            menuPortalTarget={
                props.portalTarget &&
                typeof window !== 'undefined' &&
                document.body
            }
        />
    );
}

export { CustomSelect };
