import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import classNames from 'classnames';
import styles from './Checkbox.module.scss';
import { IOptions } from '../../types/types';

interface CheckboxProps {
    text: string;
    options: IOptions;
    setOptions: any;
    flag: boolean;
}

function Checkbox({ text, options, setOptions, flag }: CheckboxProps) {
    const [checked, setChecked] = useState(false);

    const router = useRouter();

    const chengeCheckbox = () => {
        setChecked(!checked);
        let copy = [...options.filter.checkbox];
        const index = copy.indexOf(text);
        if (index === -1) {
            copy = [...copy, text];
        } else copy.splice(index, 1);
        setOptions({
            ...options,
            filter: {
                ...options.filter,
                checkbox: [...copy],
            },
        });
        flag &&
            router.push(
                {
                    pathname: `/${router.query.id}`,
                    query: {
                        ...router.query,
                        checkbox: copy.join('-'),
                    },
                },
                undefined,
                { scroll: false, shallow: true }
            );
    };

    useEffect(() => {
        setChecked(options.filter.checkbox.indexOf(text) !== -1);
    }, [options]);

    return (
        <label className={classNames('text', styles.checkbox)}>
            <input
                type="checkbox"
                checked={checked}
                onChange={chengeCheckbox}
            />
            <span></span>
            {text}
        </label>
    );
}

export { Checkbox };
