import classNames from 'classnames';
import styles from './Search.module.scss';

interface SearchProps {
    title: string;
    text: string;
    st?: string;
    style?: string;
}

function Search({ title, text, st, style }: SearchProps) {
    return (
        <section className={classNames(styles.search, style)}>
            <div className="container">
                <h2 className={classNames('subTitle', styles.searchTitle, st)}>
                    {title}
                </h2>
                <p className={styles.searchText}>{text}</p>
                <button
                    className={classNames('btn', 'icon-1', styles.searchBtn)}
                >
                    Открыть карту
                </button>
            </div>
        </section>
    );
}

export { Search };
