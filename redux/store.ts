import { configureStore } from '@reduxjs/toolkit';
import { createWrapper } from 'next-redux-wrapper';
import sdaemReducer from './sdaemSlice';

export function makeStore() {
    return configureStore({
        reducer: {
            sdaem: sdaemReducer,
        },
    });
}

export const wrapper = createWrapper(makeStore, { debug: true });
