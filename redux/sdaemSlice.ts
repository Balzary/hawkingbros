import { createSlice } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';

const sdaemSlice = createSlice({
    name: 'sdaem',
    initialState: {
        dataUser: '',
    },
    reducers: {
        setDataUser(state, action) {
            state.dataUser = action.payload;
        },
    },
    extraReducers: {
        [HYDRATE]: (state, action) => {
            state.dataUser = action.payload.sdaem.dataUser;
        },
    },
});

export const { setDataUser } = sdaemSlice.actions;

export default sdaemSlice.reducer;
