import { IOptions, IApartments } from '../types/types';
import { useMemo, useRef } from 'react';

export const useFilters = (
    apartments: IApartments[],
    options: IOptions
): IApartments[] => {
    const filter = useRef<IApartments[]>([]);

    filter.current = useMemo(() => {
        const keys = Object.keys(options.filter);

        return apartments.filter((item) => {
            let flag = true;
            for (let j = 0; j < keys.length; j++) {
                //@ts-ignore
                const b = options.filter[keys[j]];
                if (keys[j] === 'isGold') {
                    if (item.isGold && !b) {
                        flag = false;
                        break;
                    }
                } else if (b) {
                    if (keys[j] === 'checkbox') {
                        for (let i = 0; i < b.length; i++) {
                            if (!item.checkbox.includes(b[i])) {
                                flag = false;
                                break;
                            }
                        }
                        if (flag === false) break;
                    } else if (keys[j] === 'from' || keys[j] === 'before') {
                        if (keys[j] === 'from' && item.price < b) {
                            flag = false;
                            break;
                        }
                        if (keys[j] === 'before' && item.price > b) {
                            flag = false;
                            break;
                        }
                        //@ts-ignore
                    } else if (item[keys[j]] !== b) {
                        flag = false;
                        break;
                    }
                }
            }
            if (flag) return item;
        });
    }, [options]);

    useMemo(() => {
        switch (options.sort) {
            case 'ASC':
                return filter.current.sort((a, b) => a.price - b.price);
            case 'DESC':
                return filter.current.sort((a, b) => b.price - a.price);
        }
    }, [options]);

    return filter.current;
};
