import styles from './Home.module.scss';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import classNames from 'classnames';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Slider from 'react-slick';
import { CustomSelect } from '../components/CustomSelect';
import { Item } from '../components/Item';
import { Search } from '../components/Search';
import { Filters } from '../components/Filters';
import { useFilters } from '../hooks/useFilters';
import { rooms, value } from '../data/settings';
import $api from '../http/index';
import { ICard, IApartments } from '../types/types';
import { getAreas, getMetro, getCities } from '../utils/index';
import { declination } from '../utils';

interface HomeProps {
    cards: ICard[];
    apartments: IApartments[];
}

interface ISelect {
    value: string;
    label: string;
}

export default function Home({ cards, apartments }: HomeProps) {
    const citie = 'Минск'; //город, который будем отображать на главной, можно поменять, к примеру на Витебск

    const [cities, setCities] = useState<ISelect[]>([]);
    const [citieActive, setCitieActive] = useState<ISelect>();
    const [areas, setAreas] = useState<ISelect[]>([]);
    const [areaActive, setAreaActive] = useState({
        value: '',
        label: '',
    });
    const [metro, setMetro] = useState<ISelect[]>([]);
    const [metroActive, setMetroActive] = useState({
        value: '',
        label: '',
    });
    const [active, setActive] = useState(0); //табы
    const [activeFilters, setActiveFilters] = useState(false); //доп. фильтры

    const [inpFrom, setInpFrom] = useState('');
    const [inpBefore, setInpBefore] = useState('');

    const [reset2, setReset2] = useState(false); //для сброса метро и района после изменения города

    //@ts-ignore
    const { dataUser } = useSelector((state) => state.sdaem);

    const [options, setOptions] = useState({
        ...value,
        filter: {
            ...value.filter,
            isGold: dataUser.isGold,
        },
    });
    const [options2, setOptions2] = useState({
        ...value,
        filter: {
            ...value.filter,
            isGold: dataUser.isGold,
        },
    });

    useEffect(() => {
        setOptions({
            ...value,
            filter: { ...value.filter, isGold: dataUser.isGold },
        });
    }, [apartments]);

    const settings = {
        dots: false,
        arrows: true,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
            {
                breakpoint: 670,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    const tabs = [
        'Квартиры на сутки',
        'Коттеджи и усадьбы',
        'Бани и сауны',
        'Авто напрокат',
    ];

    useEffect(() => {
        setCities(getCities());
        setAreas(getAreas(citie));
    }, []);

    const filteredCards = //@ts-ignore
        apartments[citie] ? useFilters(apartments[citie], options) : [];

    useEffect(() => {
        areaActive.value && setMetro(getMetro(citie, areaActive.value));
    }, [areaActive]);

    useEffect(() => {
        if (metroActive.value || areaActive.value) {
            setOptions({
                ...options,
                filter: {
                    ...options.filter,
                    metro: metroActive.value,
                    area: areaActive.value,
                },
            });
        }
    }, [metroActive, areaActive]);

    const openTab = (e: any) => setActive(+e.target.dataset.index);

    const router = useRouter();

    interface IQuery {
        checkbox?: string;
    }

    //пушим в строку поиска выбранные фильтры
    const clickHandler = () => {
        const query: IQuery = {};
        Object.keys(options2.filter).map((key) => {
            if (key !== 'isGold') {
                if (key === 'checkbox') {
                    options2.filter[key].map((item) => {
                        if (query.checkbox) {
                            query.checkbox = query.checkbox + '-' + item;
                        } else query.checkbox = item;
                    });
                } else {
                    //@ts-ignore
                    if (options2.filter[key]) {
                        //@ts-ignore
                        query[key] = options2.filter[key];
                    }
                }
            }
        });
        router.push({
            pathname: `/${citieActive?.value}`,
            query: { ...query },
        });
    };

    return (
        <>
            <div className="container">
                <section className={styles.intro}>
                    <h1 className={classNames('title', styles.introTitle)}>
                        Sdaem.by - у нас живут <span>ваши объявления</span>
                    </h1>
                    <div className={classNames(styles.tabs, 'tabs')}>
                        <div className={styles.tab}>
                            {tabs.map((item, i) => (
                                <span
                                    key={i}
                                    data-index={i}
                                    className={classNames(
                                        styles.tabItem,
                                        i === active ? styles.tabItemActive : ''
                                    )}
                                    onClick={openTab}
                                >
                                    {item}
                                </span>
                            ))}
                        </div>
                        <div
                            style={
                                0 === active
                                    ? { display: 'block' }
                                    : { display: 'none' }
                            }
                        >
                            <div className={styles.tabsBlock}>
                                <span className={styles.tabsItem}>
                                    <p
                                        className={classNames(
                                            'text',
                                            styles.tabsText
                                        )}
                                    >
                                        Город
                                    </p>
                                    <CustomSelect
                                        placeholder="Выберите"
                                        width="150px"
                                        height="37px"
                                        options={cities}
                                        portalTarget={true}
                                        value={citieActive}
                                        setValue={(e: any) => {
                                            setCitieActive(e);
                                            setReset2(true);
                                        }}
                                    />
                                </span>
                                <span className={styles.tabsItem}>
                                    <p
                                        className={classNames(
                                            'text',
                                            styles.tabsText
                                        )}
                                    >
                                        Комнаты
                                    </p>
                                    <CustomSelect
                                        placeholder="Выберите"
                                        width="150px"
                                        height="37px"
                                        options={rooms}
                                        portalTarget={true}
                                        setValue={(e: any) => {
                                            setOptions2({
                                                ...options2,
                                                filter: {
                                                    ...options2.filter,
                                                    rooms: e.value,
                                                },
                                            });
                                        }}
                                    />
                                </span>
                                <span className={classNames(styles.tabsItem)}>
                                    <p
                                        className={classNames(
                                            'text',
                                            styles.tabsText
                                        )}
                                    >
                                        Цена за сутки (BYN)
                                    </p>
                                    <input
                                        className={classNames(styles.tabsInput)}
                                        type="number"
                                        placeholder="От"
                                        value={inpFrom}
                                        onChange={(e) =>
                                            setInpFrom(e.target.value)
                                        }
                                        onBlur={() => {
                                            setOptions2({
                                                ...options2,
                                                filter: {
                                                    ...options2.filter,
                                                    from: inpFrom,
                                                },
                                            });
                                        }}
                                    />
                                    <span className={styles.tabsSpn}>-</span>
                                    <input
                                        className={classNames(styles.tabsInput)}
                                        type="number"
                                        placeholder="До"
                                        value={inpBefore}
                                        onChange={(e) =>
                                            setInpBefore(e.target.value)
                                        }
                                        onBlur={() => {
                                            setOptions2({
                                                ...options2,
                                                filter: {
                                                    ...options2.filter,
                                                    before: inpBefore,
                                                },
                                            });
                                        }}
                                    />
                                </span>
                                <span
                                    className={classNames(
                                        styles.tabsItem,
                                        activeFilters && styles.tabsItem_b
                                    )}
                                    onClick={() => {
                                        setActiveFilters(!activeFilters);
                                    }}
                                >
                                    <p
                                        className={classNames(
                                            'text',
                                            'icon-18',
                                            styles.tabText
                                        )}
                                    >
                                        Больше опций
                                    </p>
                                </span>
                                <span className={classNames(styles.tabsItem)}>
                                    <p
                                        className={classNames(
                                            'text',
                                            'icon-1',
                                            styles.tabText
                                        )}
                                    >
                                        На карте
                                    </p>
                                </span>
                                <span className={classNames(styles.tabsItem)}>
                                    <button
                                        className={classNames(
                                            styles.tabsBtn,
                                            'icon-11'
                                        )}
                                        disabled={!citieActive}
                                        onClick={() => {
                                            clickHandler();
                                        }}
                                    >
                                        Показать
                                    </button>
                                </span>
                            </div>
                            <Filters
                                active={activeFilters}
                                setOptions={setOptions2}
                                options={options2}
                                style={styles.pd}
                                citie={citieActive?.value}
                                query={router.query}
                                flag={false}
                                reset2={reset2}
                                setReset2={setReset2}
                            />
                        </div>
                    </div>
                </section>
                <section className={styles.links}>
                    <div className={styles.linksBlock}>
                        <div className={styles.linksItem}>
                            <div className={styles.linksImg}>
                                <img src="8.png" alt="" />
                            </div>
                            <div className={styles.linksOverflow}>
                                <span className={styles.linksSubTitle}>
                                    Снять квартиру
                                </span>
                                <h3
                                    className={classNames(
                                        styles.linksTitle,
                                        'subTitle'
                                    )}
                                >
                                    Квартиры на сутки
                                </h3>
                                <div className={styles.linksLabels}>
                                    {Object.keys(apartments).map((key) => (
                                        <Link key={key} href={`/${key}`}>
                                            <span className={styles.linksLabel}>
                                                {key}
                                            </span>
                                        </Link>
                                    ))}
                                </div>
                            </div>
                        </div>
                        <div className={styles.linksItem}>
                            <div className={styles.linksImg}>
                                <img src="9.png" alt="" />
                            </div>
                            <span className={styles.linksSubTitle}>
                                Снять коттедж на праздник
                            </span>
                            <h3
                                className={classNames(
                                    styles.linksTitle,
                                    'subTitle'
                                )}
                            >
                                Коттеджи и усадьбы
                            </h3>
                            <button
                                className={classNames(
                                    styles.linksBtn,
                                    'icon-11'
                                )}
                            ></button>
                        </div>
                        <div className={styles.linksItem}>
                            <div className={styles.linksImg}>
                                <img src="10.png" alt="" />
                            </div>
                            <span className={styles.linksSubTitle}>
                                Попариться в бане с друзьями
                            </span>
                            <h3
                                className={classNames(
                                    styles.linksTitle,
                                    'subTitle'
                                )}
                            >
                                Бани и сауны
                            </h3>
                            <button
                                className={classNames(
                                    styles.linksBtn,
                                    'icon-11'
                                )}
                            ></button>
                        </div>
                        <div className={styles.linksItem}>
                            <div className={styles.linksImg}>
                                <img src="11.png" alt="" />
                            </div>
                            <span className={styles.linksSubTitle}>
                                Если срочно нужна машина
                            </span>
                            <h3
                                className={classNames(
                                    styles.linksTitle,
                                    'subTitle'
                                )}
                            >
                                Авто на прокат
                            </h3>
                            <button
                                className={classNames(
                                    styles.linksBtn,
                                    'icon-11'
                                )}
                            ></button>
                        </div>
                    </div>
                    <div className={styles.lists}>
                        <div className={styles.listsBlock}>
                            <h2 className={styles.listsTitle}>Квартиры</h2>
                            <ul className={styles.listsList}>
                                {Object.keys(apartments).map((key) => (
                                    <li key={key} className={styles.listsItem}>
                                        <Link
                                            href={`/${key}`}
                                            className={classNames(
                                                styles.listsLink,
                                                'text'
                                            )}
                                        >
                                            Квартиры в{' '}
                                            {declination(key, 'предложный')}
                                        </Link>
                                        <span
                                            className={classNames(
                                                styles.listsCount,
                                                'text'
                                            )}
                                        >
                                            {
                                                //@ts-ignore
                                                apartments[key].length
                                            }
                                        </span>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <div className={styles.listsBlock}>
                            <h2 className={styles.listsTitle}>
                                Коттеджи и усадьбы
                            </h2>
                            <ul className={styles.listsList}>
                                <li className={styles.listsItem}>
                                    <a
                                        href="#"
                                        className={classNames(
                                            styles.listsLink,
                                            'text'
                                        )}
                                    >
                                        Аггроусадьбы
                                    </a>
                                    <span
                                        className={classNames(
                                            styles.listsCount,
                                            'text'
                                        )}
                                    >
                                        110
                                    </span>
                                </li>
                                <li className={styles.listsItem}>
                                    <a
                                        href="#"
                                        className={classNames(
                                            styles.listsLink,
                                            'text'
                                        )}
                                    >
                                        Коттеджи
                                    </a>
                                    <span
                                        className={classNames(
                                            styles.listsCount,
                                            'text'
                                        )}
                                    >
                                        110
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <div className={styles.listsBlock}>
                            <h2 className={styles.listsTitle}>
                                Популярные направления
                            </h2>
                            <ul className={styles.listsList}>
                                <li className={styles.listsItem}>
                                    <a
                                        href="#"
                                        className={classNames(
                                            styles.listsLink,
                                            'text'
                                        )}
                                    >
                                        Коттеджи и усадьбы на о. Брасласких
                                    </a>
                                </li>
                                <li className={styles.listsItem}>
                                    <a
                                        href="#"
                                        className={classNames(
                                            styles.listsLink,
                                            'text'
                                        )}
                                    >
                                        Коттеджи и усадьбы (жилье) на Нарочи
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
            </div>
            <section className={styles.slider}>
                <div className="container">
                    <div className={styles.sliderFlex}>
                        <div>
                            <span className={styles.linksSubTitle}>
                                КВАРТИРЫ НА СУТКИ
                            </span>
                            <h2 className={classNames('subTitle')}>
                                Аренда квартир в{' '}
                                {declination(citie, 'предложный')}
                            </h2>
                        </div>
                        <div className={styles.sliderSelect}>
                            <div className={styles.sliderSelect_f}>
                                <CustomSelect
                                    placeholder="Район"
                                    width="185px"
                                    height="37px"
                                    options={areas}
                                    value={areaActive.value && areaActive}
                                    bg="white"
                                    shadow="0px 10px 20px rgba(0, 96, 206, 0.1)"
                                    setValue={(e: any) => {
                                        setMetroActive({
                                            value: '',
                                            label: '',
                                        });
                                        setAreaActive(e);
                                    }}
                                />
                            </div>
                            <CustomSelect
                                placeholder="Метро"
                                width="185px"
                                height="37px"
                                options={metro}
                                disabled={metro.length === 0}
                                value={metroActive.value && metroActive}
                                setValue={setMetroActive}
                                bg="white"
                                shadow="0px 10px 20px rgba(0, 96, 206, 0.1)"
                            />
                        </div>
                    </div>
                    <div className={styles.sliderBlock}>
                        {filteredCards.length > 0 ? (
                            <Slider {...settings}>
                                {filteredCards.map((apartment) => (
                                    <Item
                                        key={apartment.id}
                                        apartment={apartment}
                                        isGold={apartment.isGold}
                                    />
                                ))}
                            </Slider>
                        ) : (
                            <p className={classNames('text', styles.sliderP)}>
                                Ничего не найдено
                            </p>
                        )}
                    </div>
                    <div className={styles.sliderWrapper}>
                        <div className={styles.sliderTotal}>
                            {filteredCards.length} <b>+</b>
                            <span className="text">
                                Предложений в {declination(citie, 'предложный')}
                            </span>
                        </div>
                        <Link href={`/${citie}`}>
                            <button
                                className={classNames('btn', styles.sliderBtn)}
                            >
                                Посмотреть все
                            </button>
                        </Link>
                    </div>
                </div>
            </section>
            <Search
                title="Поиск квартир на карте"
                text="Ищите квартиры на сутки в центре города, возле парка или в живописном районе"
                st={styles.search}
            />
            <section className={styles.gold}>
                <div className={classNames('container', styles.goldContainer)}>
                    <div className={styles.goldItem}>
                        <div className={styles.goldFlex}>
                            <div
                                className={classNames(
                                    styles.goldIcon,
                                    'icon-20'
                                )}
                            ></div>
                            <h3 className={styles.goldTitle}>
                                Начните привлекать клиентов бесплатно!
                            </h3>
                        </div>
                        <p className={classNames(styles.goldText, 'text')}>
                            Пройдя простую регистрацию на сайте у Вас появится
                            личный кабинет, в котором возможно{' '}
                            <b>бесплатно создавать и публиковать</b> объявления
                            на сайте.
                        </p>
                        <button className={classNames('btn', styles.goldBtn)}>
                            + Разместить объявление
                        </button>
                    </div>
                    <div className={styles.goldItem}>
                        <div className={styles.goldFlex}>
                            <div
                                className={classNames(
                                    styles.goldIcon,
                                    'icon-21'
                                )}
                            ></div>
                            <h3 className={styles.goldTitle}>
                                Поднимайте объявления
                            </h3>
                        </div>
                        <p className={classNames(styles.goldText, 'text')}>
                            Вы в любое время можете <b>поднимать</b> объявления{' '}
                            <b>вверх первой страницы</b> каталога, они
                            разместятся сразу после платных объявлений до тех
                            пор, пока другой пользователь не повторит процедуру.
                        </p>
                        <button className={classNames('btn', styles.goldBtn)}>
                            Узнать стоимость услуги
                        </button>
                    </div>
                    <div
                        className={classNames(
                            styles.goldItem,
                            styles.goldItem_g
                        )}
                    >
                        <h3
                            className={classNames(
                                styles.goldTitle,
                                styles.goldTitle_g
                            )}
                        >
                            Приоритет Gold
                        </h3>
                        <p className={classNames(styles.goldText, 'text')}>
                            Приоритетное размещение <b>Gold</b> позволяет{' '}
                            <b>закрепить ваше объявление</b> в верхней части
                            каталога!
                            <br />
                            <br />
                            <span>
                                Gold объявления <b>перемещаются каждые 5 мин</b>{' '}
                                на 1 позицию, что делает размещение одинаковым
                                для всех.
                            </span>
                        </p>
                        <button
                            className={classNames(
                                'btn',
                                styles.goldBtn,
                                styles.goldBtn_g
                            )}
                        >
                            Узнать стоимость услуги
                        </button>
                    </div>
                </div>
            </section>
            <section className={styles.sdaem}>
                <div className={classNames('container', styles.sdaemContainer)}>
                    <div className={styles.sdaemAbout}>
                        <span className={styles.linksSubTitle}>
                            ЧТО ТАКОЕ SDAEM.BY
                        </span>
                        <h2
                            className={classNames(
                                'subTitle',
                                styles.sdaemTitle
                            )}
                        >
                            Квартира на сутки в{' '}
                            {declination(citie, 'предложный')}
                        </h2>
                        <div className={styles.sdaemImage}>
                            <img src="12.png" alt="" />
                        </div>
                        <p className={styles.sdaemText}>
                            <span
                                style={{ fontSize: '16px', fontWeight: '600' }}
                            >
                                Нужна квартира на сутки в{' '}
                                {declination(citie, 'предложный')}?
                            </span>
                            <br />
                            На веб-сайте sdaem.by вас ждет масса выгодных
                            предложений. Каталог насчитывает более 500 квартир.
                            Благодаря удобной навигации вы быстро найдете
                            подходящий вариант.
                            <br />
                            <br />
                            В каталоге представлены комфортабельные
                            однокомнатные квартиры на сутки и квартиры с большим
                            количеством комнат в разных районах города, с
                            различной степенью удобства от дешевых до VIP с
                            джакузи.
                            <br />
                            <br />
                            Чтобы снять квартиру на сутки в{' '}
                            {declination(citie, 'предложный')}, вам достаточно
                            определиться с выбором и связаться с владельцем для
                            уточнения условий аренды и заключить договор.
                            Заметим, на сайте представлены исключительно
                            квартиры на сутки без посредников, что избавляет
                            посетителей от необходимости взаимодействовать с
                            агентствами, тратя свое время и деньги. Также
                            пользователи сайта могут совершенно бесплатно
                            размещать объявления о готовности сдать квартиру на
                            сутки.
                        </p>
                    </div>
                    <div className={styles.sdaemNews}>
                        <h2
                            className={classNames(
                                'subTitle',
                                styles.sdaemTitle
                            )}
                        >
                            Новости
                        </h2>
                        <div className={styles.sdaemWrapper}>
                            {cards.length > 0 &&
                                cards.map((item) => (
                                    <div
                                        key={item.id}
                                        className={styles.sdaemBlock}
                                    >
                                        <Link href={`/news/${item.id}`}>
                                            <h3
                                                className={styles.sdaemSubTitle}
                                            >
                                                {item.title}
                                            </h3>
                                        </Link>
                                        <div className={styles.sdaemDate}>
                                            {item.date}
                                        </div>
                                    </div>
                                ))}
                        </div>
                        <Link
                            href={`/news`}
                            className={classNames(
                                'text',
                                'icon-11',
                                styles.sdaemLink
                            )}
                        >
                            Посмотреть все
                        </Link>
                    </div>
                </div>
            </section>
        </>
    );
}

export const getServerSideProps = async () => {
    try {
        const res = await $api.get('/cards');
        const res2 = await $api.get('/apartments');
        const cards: ICard[] = res.data.slice(0, 5);
        const apartments: IApartments[] = res2.data;
        return {
            props: {
                cards,
                apartments,
            },
        };
    } catch (err) {
        console.log(err);
    }
    return {
        props: {
            cards: [],
            apartments: [],
        },
    };
};
