import { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';
import { Crumbs } from '../../components/Crumbs';
import { Item } from '../../components/Item';
import { Oval } from 'react-loader-spinner';
import { Search } from '../../components/Search';
import styles from '../Home.module.scss';
import stylesNews from '../news/News.module.scss';
import { IApartments } from '../../types/types';

export default function Favorites() {
    const [cards, setCards] = useState<IApartments[]>([]);
    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(1);

    const crumbsItems = [
        {
            id: 1,
            href: `/favorites`,
            name: `Закладки`,
        },
    ];

    const pageQty = Math.ceil(cards.length / 6);

    useEffect(() => {
        const storageItem = localStorage.getItem('apartment');
        if (storageItem) {
            const storage = JSON.parse(storageItem);
            setCards(storage);
        }
        setLoading(false);
    }, []);

    return (
        <>
            <div className={styles.header}>
                <div className="container">
                    <Crumbs crumbsItems={crumbsItems} />
                    <h1 className="subTitle">Закладки</h1>
                </div>
            </div>
            <div className={styles.apartments}>
                <div className="container">
                    {loading ? (
                        <Oval
                            height={44}
                            width={44}
                            color="#664EF9"
                            wrapperStyle={{ justifyContent: 'center' }}
                            wrapperClass=""
                            visible={true}
                            ariaLabel="oval-loading"
                            secondaryColor="#7a7a7a"
                            strokeWidth={2}
                            strokeWidthSecondary={2}
                        />
                    ) : (
                        <>
                            <h2 className={styles.apartmentsTitle}>
                                {cards.length > 0
                                    ? `Найдено ${cards.length} результата`
                                    : 'Закладки отсутствуют'}
                            </h2>
                            <div className={styles.apartmentsGrid}>
                                {cards.length > 0 &&
                                    cards
                                        .slice(page * 6 - 6, page * 6)
                                        .map((apartment) => (
                                            <Item
                                                key={apartment.id}
                                                apartment={apartment}
                                                style={styles.apartmentsItem}
                                                isGold={apartment.isGold}
                                            />
                                        ))}
                            </div>
                        </>
                    )}
                    {pageQty > 0 && (
                        <ReactPaginate
                            className={stylesNews.paginate}
                            breakLabel="..."
                            nextLabel=""
                            onPageChange={(e) => setPage(e.selected + 1)}
                            pageRangeDisplayed={3}
                            pageCount={pageQty}
                            previousLabel=""
                            renderOnZeroPageCount={() => null}
                        />
                    )}
                </div>
            </div>
            <Search
                title="Показать найденные квартиры на карте"
                text="Ищите новостройки рядом с работой, парком или родственниками"
                style={styles.searchPd}
            />
        </>
    );
}
