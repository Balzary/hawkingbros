import type { AppProps } from 'next/app';
import NextNprogress from 'nextjs-progressbar';
import { Provider } from 'react-redux';
import { parseCookies } from 'nookies';
import { wrapper } from '../redux/store';
import { setDataUser } from '../redux/sdaemSlice';
import Layout from '../components/Layout';
import '../styles/globals.scss';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import $api from '../http/index';

const App = ({ Component, ...rest }: AppProps) => {
    const { store, props } = wrapper.useWrappedStore(rest);
    return (
        <Provider store={store}>
            <Layout>
                {/*<NextNprogress color="#ffd54f" />*/}
                <Component {...props.pageProps} />
            </Layout>
        </Provider>
    );
};

App.getInitialProps = wrapper.getInitialAppProps(
    (store) =>
        async ({ ctx, Component }) => {
            try {
                const { token } = parseCookies(ctx);
                const res = await $api.post('/refresh', {
                    token: token,
                });
                store.dispatch(setDataUser(res.data));
            } catch (err) {
                console.log(err);
            }
            return {
                pageProps: Component.getInitialProps
                    ? await Component.getInitialProps({ ...ctx, store })
                    : {},
            };
        }
);

export default App;
