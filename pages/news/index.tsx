import { useEffect, useState, useMemo, useRef } from 'react';
import ReactPaginate from 'react-paginate';
import classNames from 'classnames';
import { Crumbs } from '../../components/Crumbs';
import { Card } from '../../components/pages/Card';
import { Search } from '../../components/pages/Search';
import styles from './News.module.scss';
import $api from '../../http';
import { ICard } from '../../types/types';

interface NewsProps {
    cards: ICard[];
}

interface IOptions {
    filter: {
        value: string;
    };
}

export default function News({ cards }: NewsProps) {
    const [options, setOptions] = useState({
        filter: {
            value: '',
        },
    });
    const [page, setPage] = useState(1);

    const crumbsItems = [
        {
            id: 1,
            href: '/news',
            name: 'Новости',
        },
    ];

    const filters = (cards: ICard[], options: IOptions) => {
        const filter = useRef<ICard[]>();
        filter.current = useMemo(() => {
            return cards.filter((item) => {
                return item.title
                    .toLowerCase()
                    .includes(options.filter.value.toLowerCase());
            });
        }, [options.filter]);
        return filter.current;
    };

    const filteredCards = filters(cards, options);
    const pageQty = Math.ceil(filteredCards.length / 9);

    useEffect(() => {
        if (filteredCards.length > 0) {
            setPage(1);
            const firstPage = document.querySelector(
                '.News_paginate__bzaCf'
            ) as HTMLDivElement;
            const el = firstPage.childNodes[1].firstChild as HTMLElement;
            el.click();
        }
    }, [filteredCards]);

    return (
        <div className={classNames(styles.news, 'container')}>
            <Crumbs crumbsItems={crumbsItems} />
            <div className={styles.newsFlex} id="news">
                <h1 className={classNames(styles.newsTitle, 'title')}>
                    Новости
                </h1>
                <Search options={options} setOptions={setOptions} />
            </div>
            <div className={styles.newsCards}>
                {filteredCards.length > 0 ? (
                    filteredCards
                        .slice(page * 9 - 9, page * 9)
                        .map((item) => <Card key={item.id} card={item} />)
                ) : (
                    <p className="text">Ничего не найдено</p>
                )}
            </div>
            {pageQty > 0 && (
                <ReactPaginate
                    className={styles.paginate}
                    breakLabel="..."
                    nextLabel=""
                    onPageChange={(e) => {
                        setPage(e.selected + 1);
                        const news = document.querySelector(
                            '#news'
                        ) as HTMLDivElement;
                        news.scrollIntoView({
                            behavior: 'smooth',
                            block: 'end',
                        });
                    }}
                    pageRangeDisplayed={3}
                    pageCount={pageQty}
                    previousLabel=""
                    renderOnZeroPageCount={() => null}
                />
            )}
        </div>
    );
}

export const getServerSideProps = async () => {
    try {
        const res = await $api.get('/cards');
        const cards: ICard[] = res.data;
        return {
            props: {
                cards,
            },
        };
    } catch (err) {
        console.log(err);
    }
    return {
        props: {
            cards: [],
        },
    };
};
