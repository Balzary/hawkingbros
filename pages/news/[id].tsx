import classNames from 'classnames';
import { NextPageContext } from 'next';
import { Crumbs } from '../../components/Crumbs';
import { Card } from '../../components/pages/Card';
import { SocialLinks } from '../../components/SocialLinks';
import styles from './News.module.scss';
import stylesCard from '../../components/pages/Card/Card.module.scss';
import $api from '../../http/index';
import { ICard } from '../../types/types';

interface OneNewsProps {
    card: ICard;
    cards: ICard[];
}

export default function OneNews({ card, cards }: OneNewsProps) {
    const crumbsItems = [
        {
            id: 1,
            href: '/news',
            name: 'Новости',
        },
        {
            id: 2,
            href: card && `/news/${card.id}`,
            name: card && card.title,
        },
    ];

    return (
        <>
            {
                <>
                    <div className={styles.fnHead}>
                        <div className={styles.container}>
                            <Crumbs crumbsItems={crumbsItems} />
                            <h1 className={classNames('title', styles.title)}>
                                {card.title}
                            </h1>
                            <div className={styles.fnBlock}>
                                <span
                                    style={{
                                        background: 'rgba(102, 78, 249, 0.1)',
                                        color: '#664ef9',
                                    }}
                                    className={classNames(
                                        stylesCard.cardDate,
                                        styles.newsDate
                                    )}
                                >
                                    {card.date}
                                </span>
                                <SocialLinks bgColor="rgba(102, 78, 249, 0.1)" />
                            </div>
                        </div>
                    </div>
                    <div
                        className={styles.container}
                        style={{ marginTop: '-44px' }}
                    >
                        <div className={styles.fnImg}>
                            <img src={card.img} />
                        </div>
                        <div className={styles.text}>
                            {card.fullText.map((item, index) => (
                                <p key={index}>{item}</p>
                            ))}
                        </div>
                    </div>
                </>
            }
            {cards.length > 0 ? (
                <div className={styles.moreNews}>
                    <div className="container">
                        <h2 className={styles.fnTitle}>Читайте также</h2>
                        <div
                            className={styles.newsCards}
                            style={{ marginBottom: '0' }}
                        >
                            {cards.map((item) => (
                                <Card key={item.id} card={item} />
                            ))}
                        </div>
                    </div>
                </div>
            ) : (
                ''
            )}
        </>
    );
}

export const getServerSideProps = async ({ query }: NextPageContext) => {
    try {
        const res = await $api.get(`/cards/${query.id}`);
        const data: OneNewsProps = res.data;
        return {
            props: {
                card: data.card,
                cards: data.cards,
            },
        };
    } catch (err) {
        console.log(err);
    }
    return {
        notFound: true,
    };
};
