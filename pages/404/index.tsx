import Link from 'next/link';
import classNames from 'classnames';
import styles from './404.module.scss';

export default function errorPage() {
    return (
        <div className={styles.error404}>
            <div className={styles.container}>
                <div className={styles.block}>
                    <h1 className={styles.title}>Ошибка 404</h1>
                    <p className={styles.text}>
                        Возможно, у вас опечатка в адресе страницы, или её
                        просто не существует
                    </p>
                    <Link href="/">
                        <button className={classNames(styles.btn, 'icon-2')}>
                            Вернуться на главную
                        </button>
                    </Link>
                </div>
            </div>
        </div>
    );
}
