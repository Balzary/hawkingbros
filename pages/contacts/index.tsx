import { useState } from 'react';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { Modal } from '../../components/Modal';
import styles from './Contacts.module.scss';
import stylesInput from '../../components/Input/Input.module.scss';

export default function Contacts() {
    const [success, setSuccess] = useState(false);

    const socialItems = [
        {
            id: 1,
            href: '#',
            icon: 'icon-4',
            styleIcon: styles.icon4,
        },
        {
            id: 2,
            href: '#',
            icon: 'icon-5',
            styleIcon: styles.icon5,
        },
        {
            id: 3,
            href: '#',
            icon: 'icon-6',
            styleIcon: styles.icon6,
        },
    ];

    const {
        register,
        handleSubmit,
        formState: { isValid, errors },
    } = useForm({
        defaultValues: {
            name: '',
            email: '',
            message: '',
        },
        mode: 'onBlur',
    });

    const onSubmit = async (values: { [key: string]: string }) => {
        try {
            setSuccess(true);
        } catch (e: any) {
            console.log(e.response?.data?.message);
        }
    };

    return (
        <div className={styles.contacts}>
            <div className={styles.blur}>
                <div className={classNames('container', styles.flex)}>
                    <div className={styles.content}>
                        <h1 className={classNames('title', styles.title)}>
                            Контакты
                        </h1>
                        <p className={styles.text}>
                            Если у Вас есть пожелания, предложения или претензии
                            по организации работы сайта мы всегда рады услышать
                            Ваше мнение.
                        </p>
                        <ul className={styles.list}>
                            <li className={classNames(styles.listItem)}>
                                <span
                                    className={classNames(
                                        'icon-1',
                                        styles.icon1
                                    )}
                                ></span>
                                220068, РБ, г. Минск, ул. Осипенко, 21, кв.23
                            </li>
                            <li className={classNames(styles.listItem)}>
                                <span
                                    className={classNames(
                                        'icon-14',
                                        styles.icon14
                                    )}
                                ></span>
                                <a
                                    className={styles.tel}
                                    href="tel:+37529621-48-33"
                                >
                                    +375 29 621-48-33
                                </a>
                                <a href="#">
                                    <span
                                        className={classNames(
                                            'icon-8',
                                            styles.icon8
                                        )}
                                    ></span>
                                </a>
                                <a href="#">
                                    <span
                                        className={classNames(
                                            'icon-9',
                                            styles.icon9
                                        )}
                                    ></span>
                                </a>
                                <a href="#">
                                    <span
                                        className={classNames(
                                            'icon-10',
                                            styles.icon10
                                        )}
                                    ></span>
                                </a>
                            </li>
                            <li className={classNames(styles.listItem)}>
                                <span
                                    className={classNames(
                                        'icon-12',
                                        styles.icon12
                                    )}
                                ></span>
                                <a href="mailto:sdaem@sdaem.by">
                                    sdaem@sdaem.by
                                </a>
                            </li>
                            <li className={classNames(styles.listItem)}>
                                <span
                                    className={classNames(
                                        'icon-15',
                                        styles.icon15
                                    )}
                                ></span>
                                Режим работы: 08:00-22:00
                            </li>
                        </ul>
                        <p className={classNames('text', styles.smallText)}>
                            ИП Шушкевич Андрей Викторович УНП 192602485 Минским
                            <br />
                            горисполкомом 10.02.2016
                        </p>
                        <span
                            className={classNames(
                                styles.contentBlock,
                                'icon-17',
                                styles.icon17
                            )}
                        >
                            <p
                                className={styles.text}
                                style={{ marginBottom: '0' }}
                            >
                                Администрация сайта не владеет информацией о
                                наличии свободных квартир
                            </p>
                        </span>
                    </div>
                    <div className={styles.form}>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className={styles.formFlex}>
                                <div
                                    className={classNames(
                                        styles.formLabel,
                                        'text'
                                    )}
                                >
                                    Ваше имя
                                    <div
                                        className={classNames(
                                            'icon-13',
                                            stylesInput.inputIcon
                                        )}
                                        style={
                                            errors?.name && {
                                                border: '2px solid #EB5757',
                                            }
                                        }
                                    >
                                        <input
                                            className={stylesInput.input}
                                            placeholder="Алексей"
                                            {...register('name', {
                                                required: true,
                                            })}
                                        />
                                    </div>
                                </div>
                                <div
                                    className={classNames(
                                        styles.formLabel,
                                        'text'
                                    )}
                                >
                                    Ваша электронная почта
                                    <div
                                        className={classNames(
                                            'icon-12',
                                            stylesInput.inputIcon
                                        )}
                                        style={
                                            errors?.email && {
                                                border: '2px solid #EB5757',
                                            }
                                        }
                                    >
                                        <input
                                            className={stylesInput.input}
                                            type="email"
                                            placeholder="Введите"
                                            {...register('email', {
                                                required: true,
                                                pattern: {
                                                    value: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                                    message:
                                                        'Введите корректный email',
                                                },
                                            })}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div
                                className={classNames(styles.formLabel, 'text')}
                                style={{ marginBottom: '40px' }}
                            >
                                Ваше сообщение
                                <div
                                    className={classNames(
                                        stylesInput.inputIcon
                                    )}
                                    style={
                                        errors?.message && {
                                            border: '2px solid #EB5757',
                                        }
                                    }
                                >
                                    <textarea
                                        className={classNames(
                                            stylesInput.input,
                                            styles.formArea
                                        )}
                                        placeholder="Сообщение"
                                        {...register('message', {
                                            required: true,
                                        })}
                                    ></textarea>
                                </div>
                            </div>
                            {Object.keys(errors).length !== 0 && (
                                <div className={styles.error}>Ошибка ввода</div>
                            )}
                            <button
                                className={classNames(styles.btn)}
                                type="submit"
                                disabled={!isValid}
                            >
                                Отправить
                            </button>
                        </form>
                        <ul className={styles.socialLinks}>
                            {socialItems.map((item) => (
                                <li key={item.id} className={styles.socialItem}>
                                    <a
                                        href={item.href}
                                        className={classNames(
                                            styles.socialLink,
                                            item.icon,
                                            item.styleIcon
                                        )}
                                    ></a>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
            <Modal active={success} setActive={setSuccess} closeIcon={true}>
                <div className={styles.modal}>
                    <h2 className={classNames('subTitle', styles.modalTitle)}>
                        Ваше письмо отправлено!
                    </h2>
                    <p className={classNames('text', styles.modalText)}>
                        Какое-то сообщение о том, что письмо отправлено,
                        какое-то сообщение, что письмо отправлено.
                    </p>
                    <button
                        className={classNames(styles.btn, styles.btnYellow)}
                        onClick={() => setSuccess(false)}
                    >
                        Закрыть окно
                    </button>
                </div>
            </Modal>
        </div>
    );
}
