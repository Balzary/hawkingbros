import { NextApiRequest, NextApiResponse } from 'next';
import data from '../../../data/data.json';
import { ICard } from '../../../types/types';

export default (req: NextApiRequest, res: NextApiResponse) => {
    const filtered = data.cards.filter((p) => String(p.id) === req.query.id);

    let arr: ICard[] = [];
    let max = data.cards.length;
    let rundomnumber: number;

    //формируем массив случайных элементов
    if (max > 3) {
        while (arr.length < 3) {
            rundomnumber = Math.floor(Math.random() * max);
            arr.indexOf(data.cards[rundomnumber]) === -1 &&
                data.cards[rundomnumber] !== filtered[0] &&
                arr.push(data.cards[rundomnumber]);
        }
    }

    return filtered.length > 0
        ? res.status(200).json({ card: filtered[0], cards: arr })
        : res
              .status(404)
              .json({ message: `Новость с id ${req.query.id} не найдена.` });
};
