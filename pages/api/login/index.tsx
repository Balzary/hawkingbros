import { NextApiRequest, NextApiResponse } from 'next';
import data from '../../../data/data.json';

export default (req: NextApiRequest, res: NextApiResponse) => {
    const { name, password } = req.body;
    const user = data.users.find((user) => user.email === name);

    user && user.password === password
        ? res.status(200).json(user)
        : res.status(400).json({ message: `Неверный логин или пароль` });
};
