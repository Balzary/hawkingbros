import { NextApiRequest, NextApiResponse } from 'next';
import data from '../../../data/data.json';

export default (req: NextApiRequest, res: NextApiResponse) => {
    const { token } = req.body;
    const user = data.users.find((user) => user.token === token);

    user
        ? res.status(200).json(user)
        : res.status(400).json({ message: `Пользователь не авторизован` });
};
