import { useEffect, useState, useRef } from 'react';
import ReactPaginate from 'react-paginate';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import classNames from 'classnames';
import { NextPageContext } from 'next';
import { Crumbs } from '../components/Crumbs';
import { Item } from '../components/Item';
import { SocialLinks } from '../components/SocialLinks';
import { Search } from '../components/Search';
import { CustomSelect } from '../components/CustomSelect';
import { Filters } from '../components/Filters';
import $api from '../http/index';
import { useFilters } from '../hooks/useFilters';
import { rooms, value, sort } from '../data/settings';
import styles from './Home.module.scss';
import stylesNews from '../pages/news/News.module.scss';
import { IOptions, IApartments } from '../types/types';
import { declination } from '../utils';

interface CitieProps {
    apartments: IApartments[];
    value2: IOptions;
    query: any;
}

export default function Citie({ apartments, value2, query }: CitieProps) {
    const [page, setPage] = useState(1);
    const [isList, setIsList] = useState(false);
    const [inpFrom, setInpFrom] = useState(query.from);
    const [inpBefore, setInpBefore] = useState(query.before);
    const [active, setActive] = useState(false); //доп. фильтры
    const [reset, setReset] = useState(false);

    const [sortActive, setSortActive] = useState(
        query.sort === 'ASC' ? sort[0] : query.sort === 'DESC' && sort[1]
    );
    const [activeRoom, setActiveRoom] = useState(rooms[query.rooms - 1]);

    const [labelActive, setLabelActive] = useState(''); //метки (теги)
    const [keyActive, setKeyActive] = useState('');

    //@ts-ignore
    const { dataUser } = useSelector((state) => state.sdaem);

    const [options, setOptions] = useState({
        ...value2,
        filter: { ...value2.filter, isGold: dataUser.isGold },
    });

    const router = useRouter();

    const filtersRef = useRef<HTMLDivElement | null>(null);

    const resertFilters = () => {
        setInpFrom('');
        setInpBefore('');
        //@ts-ignore
        setSortActive('');
        //@ts-ignore
        setActiveRoom('');
        setLabelActive('');
        setReset(true);
    };

    const crumbsItems = [
        {
            id: 1,
            href: `/${query.id}`,
            name: `Квартиры в ${declination(query.id, 'предложный')}`,
        },
    ];

    const labels = {
        before: [{ value: 'Недорогие', filter: 800 }],
        from: [{ value: 'Дорогие', filter: 1200 }],
        rooms: [
            { value: '1-комнатные', filter: '1' },
            { value: '2-комнатные', filter: '2' },
            { value: '3-комнатные', filter: '3' },
            { value: '4-комнатные', filter: '4' },
            { value: '5-комнатные', filter: '5' },
        ],
        area:
            query.id === 'Минск'
                ? [
                      { value: 'Заводской р.', filter: 'Заводской' },
                      { value: 'Ленинский р.', filter: 'Ленинский' },
                      { value: 'Московский р.', filter: 'Московский' },
                      { value: 'Октябрьский р.', filter: 'Октябрьский' },
                      { value: 'Партизанский р.', filter: 'Партизанский' },
                      { value: 'Первомайский р.', filter: 'Первомайский' },
                      { value: 'Советский р.', filter: 'Советский' },
                      { value: 'Фрунзенский р.', filter: 'Фрунзенский' },
                      { value: 'Центральный р.', filter: 'Центральный' },
                  ]
                : [],
    };

    const filteredCards = useFilters(apartments, options);
    const pageQty = Math.ceil(filteredCards.length / 6);

    useEffect(() => {
        if (filteredCards.length > 0) {
            setPage(1);
            const firstPage = document.querySelector(
                '.News_paginate__bzaCf'
            ) as HTMLDivElement;
            const el = firstPage.childNodes[1].firstChild as HTMLElement;
            el.click();
        }
    }, [filteredCards]);

    useEffect(() => {
        Object.keys(query).length > 1 ? '' : resertFilters();
        setOptions({
            ...value2,
            filter: { ...value2.filter, isGold: dataUser.isGold },
        });
        if (filtersRef.current) filtersRef.current.style.pointerEvents = 'auto';
    }, [apartments]);

    return (
        <>
            <div className={styles.header}>
                <div className="container">
                    <Crumbs crumbsItems={crumbsItems} />
                    <h1 className="subTitle" style={{ marginBottom: '25px' }}>
                        Аренда квартир на сутки в{' '}
                        {declination(query.id, 'предложный')}
                    </h1>
                    <div
                        className={styles.labels}
                        style={
                            labelActive
                                ? { display: 'none' }
                                : { display: 'flex' }
                        }
                    >
                        {Object.keys(labels).map((key) =>
                            //@ts-ignore
                            labels[key].map((item, index) => (
                                <span
                                    key={index}
                                    className={classNames(
                                        'text',
                                        styles.labelsItem
                                    )}
                                    onClick={() => {
                                        resertFilters();
                                        router.push(
                                            {
                                                pathname: `/${query.id}`,
                                                query: {},
                                            },
                                            undefined,
                                            { scroll: false, shallow: true }
                                        );
                                        setOptions({
                                            ...value,
                                            filter: {
                                                ...value.filter,
                                                [key]: item.filter,
                                                isGold: dataUser.isGold,
                                            },
                                        });
                                        //@ts-ignore
                                        setLabelActive(labels[key][index]);
                                        setKeyActive(key);
                                        if (filtersRef.current)
                                            filtersRef.current.style.pointerEvents =
                                                'none';
                                    }}
                                >
                                    {item.value}
                                </span>
                            ))
                        )}
                    </div>
                    {labelActive && (
                        <span
                            className={classNames('text', styles.labelsItem)}
                            style={{ display: 'inline-block' }}
                            onClick={() => {
                                setOptions({
                                    ...value,
                                    filter: {
                                        ...value.filter,
                                        [keyActive]: '',
                                        isGold: dataUser.isGold,
                                    },
                                });
                                setLabelActive('');
                                if (filtersRef.current)
                                    filtersRef.current.style.pointerEvents =
                                        'auto';
                            }}
                        >
                            {
                                //@ts-ignore
                                labelActive.value
                            }
                            <svg
                                width="10"
                                height="10"
                                viewBox="0 0 10 10"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M5.91628 5.00007L9.81017 1.10608C10.0636 0.852787 10.0636 0.443255 9.81017 0.189966C9.55688 -0.0633221 9.14735 -0.0633221 8.89406 0.189966L5.00005 4.08396L1.10617 0.189966C0.852759 -0.0633221 0.443344 -0.0633221 0.190056 0.189966C-0.0633519 0.443255 -0.0633519 0.852787 0.190056 1.10608L4.08394 5.00007L0.190056 8.89407C-0.0633519 9.14736 -0.0633519 9.55689 0.190056 9.81018C0.316285 9.93653 0.482257 10 0.648111 10C0.813965 10 0.979819 9.93653 1.10617 9.81018L5.00005 5.91618L8.89406 9.81018C9.0204 9.93653 9.18626 10 9.35211 10C9.51797 10 9.68382 9.93653 9.81017 9.81018C10.0636 9.55689 10.0636 9.14736 9.81017 8.89407L5.91628 5.00007Z"
                                    fill="#664EF9"
                                />
                            </svg>
                        </span>
                    )}
                </div>
            </div>
            <div className={styles.filters} ref={filtersRef}>
                <div
                    className={classNames('container', styles.filtersContainer)}
                    style={{ overflow: 'auto', flexWrap: 'nowrap' }}
                >
                    <div className={styles.filtersBlock}>
                        <span
                            className={classNames('text', styles.filtersText)}
                        >
                            Комнаты
                        </span>
                        <CustomSelect
                            placeholder="Выберите"
                            width="150px"
                            height="37px"
                            options={rooms}
                            value={activeRoom}
                            portalTarget={true}
                            setValue={(e: any) => {
                                router.push(
                                    {
                                        pathname: `/${query.id}`,
                                        query: {
                                            ...router.query,
                                            rooms: e.value,
                                        },
                                    },
                                    undefined,
                                    { scroll: false, shallow: true }
                                );
                                setActiveRoom(e);
                                setOptions({
                                    ...options,
                                    filter: {
                                        ...options.filter,
                                        rooms: e.value,
                                    },
                                });
                            }}
                        />
                    </div>
                    <div className={styles.filtersBlock}>
                        <span
                            className={classNames('text', styles.filtersText)}
                        >
                            Цена за сутки (BYN)
                        </span>
                        <div>
                            <input
                                className={classNames(styles.tabsInput)}
                                type="number"
                                placeholder="От"
                                value={inpFrom}
                                onChange={(e) => setInpFrom(e.target.value)}
                                onBlur={() => {
                                    router.push(
                                        {
                                            pathname: `/${query.id}`,
                                            query: {
                                                ...router.query,
                                                from: inpFrom,
                                            },
                                        },
                                        undefined,
                                        { scroll: false, shallow: true }
                                    );
                                    setOptions({
                                        ...options,
                                        filter: {
                                            ...options.filter,
                                            from: inpFrom,
                                        },
                                    });
                                }}
                            />
                            <span className={styles.tabsSpn}>-</span>
                            <input
                                className={classNames(styles.tabsInput)}
                                type="number"
                                placeholder="До"
                                value={inpBefore}
                                onChange={(e) => setInpBefore(e.target.value)}
                                onBlur={() => {
                                    router.push(
                                        {
                                            pathname: `/${query.id}`,
                                            query: {
                                                ...router.query,
                                                before: inpBefore,
                                            },
                                        },
                                        undefined,
                                        { scroll: false, shallow: true }
                                    );
                                    setOptions({
                                        ...options,
                                        filter: {
                                            ...options.filter,
                                            before: inpBefore,
                                        },
                                    });
                                }}
                            />
                        </div>
                    </div>
                    <div
                        className={classNames(
                            styles.filtersBlock,
                            active && styles.filtersBlock_b
                        )}
                    >
                        <p
                            className={classNames(
                                'text',
                                'icon-18',
                                styles.tabText
                            )}
                            onClick={() => setActive(!active)}
                        >
                            Больше опций
                        </p>
                    </div>
                    <div className={styles.filtersBlock}>
                        <button
                            className={classNames('btn')}
                            onClick={() => {
                                router.push(
                                    {
                                        pathname: `/${query.id}`,
                                        query: {},
                                    },
                                    undefined,
                                    { scroll: false, shallow: true }
                                );
                                resertFilters();
                                setOptions({
                                    ...value,
                                    filter: {
                                        ...value.filter,
                                        isGold: dataUser.isGold,
                                    },
                                });
                            }}
                        >
                            Очистить
                        </button>
                    </div>
                </div>
                <Filters
                    active={active}
                    setOptions={setOptions}
                    options={options}
                    citie={query.id}
                    query={query}
                    flag={true}
                    reset={reset}
                    setReset={setReset}
                />
            </div>
            <div className={styles.apartments}>
                <div className={classNames('container')}>
                    <div className={styles.apartmentsFilters}>
                        <CustomSelect
                            placeholder="Сортировка"
                            width="232px"
                            height="37px"
                            options={sort}
                            //@ts-ignore
                            value={sortActive}
                            setValue={(e: any) => {
                                router.push(
                                    {
                                        pathname: `/${query.id}`,
                                        query: {
                                            ...router.query,
                                            sort: e.value,
                                        },
                                    },
                                    undefined,
                                    { scroll: false, shallow: true }
                                );
                                setSortActive(e);
                                setOptions({
                                    ...options,
                                    sort: e.value,
                                });
                            }}
                        />
                        <div className={styles.apartmentsFlex}>
                            <div className={styles.apartmentsSpns}>
                                <span
                                    className={classNames(
                                        'icon-23',
                                        styles.apartmentsSpn,
                                        isList && styles.active
                                    )}
                                    onClick={() => setIsList(true)}
                                >
                                    Список
                                </span>
                                <span
                                    className={classNames(
                                        'icon-24',
                                        styles.apartmentsSpn,
                                        !isList && styles.active
                                    )}
                                    onClick={() => setIsList(false)}
                                >
                                    Плитки
                                </span>
                            </div>
                            <button
                                className={classNames(
                                    'btn',
                                    'icon-1',
                                    styles.apartmentsBtn
                                )}
                            >
                                Показать на карте
                            </button>
                        </div>
                    </div>
                    <h2 className={styles.apartmentsTitle} id="result">
                        Найдено {filteredCards.length} результата
                    </h2>
                    <div
                        className={classNames(
                            styles.apartmentsGrid,
                            isList && styles.apartmentsGrid_list
                        )}
                    >
                        {filteredCards
                            .slice(page * 6 - 6, page * 6)
                            .map((apartment) => (
                                <Item
                                    key={apartment.id}
                                    apartment={apartment}
                                    style={styles.apartmentsItem}
                                    isList={isList}
                                    isGold={apartment.isGold}
                                />
                            ))}
                    </div>
                    <div className={styles.flex}>
                        {pageQty > 0 && (
                            <ReactPaginate
                                className={stylesNews.paginate}
                                breakLabel="..."
                                nextLabel=""
                                onPageChange={(e) => {
                                    setPage(e.selected + 1);
                                    const result = document.querySelector(
                                        '#result'
                                    ) as HTMLDivElement;
                                    result.scrollIntoView({
                                        behavior: 'smooth',
                                        block: 'start',
                                    });
                                }}
                                pageRangeDisplayed={3}
                                pageCount={pageQty}
                                previousLabel=""
                                renderOnZeroPageCount={() => null}
                            />
                        )}
                        <SocialLinks bgColor="#F4F5FA" />
                    </div>
                </div>
            </div>
            <Search
                title="Показать найденные квартиры на карте"
                text="Ищите новостройки рядом с работой, парком или родственниками "
                style={styles.searchPd}
            />
        </>
    );
}

export const getServerSideProps = async ({ query }: NextPageContext) => {
    try {
        const res = await $api.get('/apartments');
        //@ts-ignore
        if (!res.data[query.id])
            return {
                notFound: true,
            };
        //@ts-ignore
        const apartments = res.data[query.id];
        const value2 = {
            ...value,
            filter: {
                ...value.filter,
            },
        };
        Object.keys(query).map((key) => {
            if (key !== 'id') {
                switch (key) {
                    case 'checkbox':
                        if (query[key])
                            //@ts-ignore
                            value2.filter[key] = query[key].split('-');
                        break;
                    case 'sort':
                        //@ts-ignore
                        value2.sort = query[key];
                        break;
                    default:
                        //@ts-ignore
                        value2.filter[key] = query[key];
                        break;
                }
            }
        });
        return {
            props: {
                apartments,
                value2,
                query,
            },
        };
    } catch (err) {
        console.log(err);
    }
};
