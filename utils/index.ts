import { data } from '../data/settings';
import RussianNouns from 'russian-nouns-js';

interface ISelect {
    value: string;
    label: string;
}

const rne = new RussianNouns.Engine();

//склоняем
export const declination = (str: string, output: string) => {
    const arr: String[] = [];
    str.split(' ').map((item) =>
        arr.push(
            rne.decline(
                {
                    text: item,
                    gender: 'общий',
                },
                output
            )[0]
        )
    );
    return arr.join(' ');
};

//получаем города
export const getCities = () => {
    const cities: ISelect[] = [];
    data.map((item) =>
        cities.push({
            value: item.name,
            label: item.name,
        })
    );
    return cities;
};

//получаем районы города
export const getAreas = (citie: string | undefined) => {
    const areas: ISelect[] = [];
    data.map((item) => {
        if (item.name === citie) {
            item.areas.map((area) => {
                citie === 'Минск'
                    ? areas.push({
                          //@ts-ignore
                          value: area.name,
                          //@ts-ignore
                          label: area.name,
                      })
                    : areas.push({
                          //@ts-ignore
                          value: area,
                          //@ts-ignore
                          label: area,
                      });
            });
        }
    });
    return areas;
};

//получаем метро района
export const getMetro = (citie: string, area: string) => {
    const metro: ISelect[] = [];
    citie === 'Минск' &&
        data.map((item) => {
            if (item.name === citie) {
                item.areas.map((itemArea) => {
                    //@ts-ignore
                    if (itemArea.name === area) {
                        //@ts-ignore
                        itemArea.metro.map((itemMetro) => {
                            metro.push({
                                value: itemMetro,
                                label: itemMetro,
                            });
                        });
                    }
                });
            }
        });
    return metro;
};
