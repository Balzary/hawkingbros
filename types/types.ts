export interface IOptions {
    sort?: string;
    filter: {
        area: string;
        metro: string;
        rooms: number;
        checkbox: string[];
        slPlaces: number;
        isGold: boolean;
        from: string;
        before: string;
    };
}

export interface IApartments {
    id: number;
    price: number;
    person: string;
    rooms: number;
    square: number;
    address: string;
    description: string;
    contacts: { [key: string]: string };
    area: string;
    metro: string;
    image: string[];
    checkbox: string[];
    slPlaces: number;
    isGold: boolean;
}

export interface ICard {
    img: string;
    title: string;
    description: string;
    date: string;
    id: number;
    fullText: string[];
}
